'use strict';

process.env.NODE_ENV = 'test';
global.chai = require('chai');
global.chai.should();

global.expect = global.chai.expect;
// global.sinon = require('sinon');

// global.sinonChai = require('sinon-chai');
// global.chai.use(global.sinonChai);

const commonService = require('./../lib/common.service');
const config = require('./../config/config');
const jwt = require('jsonwebtoken');
global.getToken = (payload) => {
    return jwt.sign(payload, config.auth.secret);
}
global.models = require('./../models');
global.Op = models.Sequelize.Op;
global.initDb = async() => {
    let result = await models.sequelize.sync({ force: true });
    const resultFacility = await models.facility.create({ name: 'facility' });
    const resultUnit = await models.units.create({ name: 'unit', facility_id: resultFacility.id });
    const resultActions = await models.patient_actions.bulkCreate([
        { name: 'Admit' },
        { name: 'Discharge' },
        { name: 'Remove' }
    ]);
    const resultPatient = await models.patients.create({
        units_id: resultUnit.id,
        first_name: 'Firstname',
        last_name: 'Lastname',
        last_four_sn: 1234,
        patient_actions_id: 1
    });
    const resultTypes = await models.call_types.bulkCreate([
        { name: 'I\'m in pain', short_name: 'Pain', icon_name: 'headache.svg', title: 'Pain', subtitle: 'I\'m in' },
        { name: 'I need the bathroom', short_name: 'Bathroom', icon_name: 'toilet.svg', title: 'Bathroom', subtitle: 'I need the' },
        { name: 'I need a drink', short_name: 'Drink', icon_name: 'glass-water.svg', title: 'Drink', subtitle: 'I need a' },
        { name: 'I have trouble with my My TV/Remote', short_name: 'TV/Remote', icon_name: 'television.svg', title: 'TV/Remove', subtitle: 'I have trouble with my' },
        { name: 'I have a Question/Concern', short_name: 'Question', icon_name: 'help.svg', title: 'Question/Concern', subtitle: 'I have a ' }
    ]);
    const resultCallActions = await models.call_actions.bulkCreate([
        { name: 'Submitted' },
        { name: 'Resolved' },
        { name: 'Canceled' },
        { name: 'Other' }
    ]);
    const resultUserRoles = await models.user_roles.bulkCreate([
        { name: 'Administartor' },
        { name: 'Nurse' }
    ]);
    const resultUserStatuses = await models.user_statuses.bulkCreate([
        { name: 'Active' },
        { name: 'Inactive' },
        { name: 'Remove' }
    ]);
    const password = '123456';
    const hash = await commonService.setHash(password);
    const resultUsers = await models.users.create({
        first_name: 'Test',
        last_name: 'User',
        email: 'test@test.com',
        password: hash,
        user_roles_id: 1,
        user_statuses_id: 1
    });
}