'use strict';

const apiDocRouter = require('./api-docs');
const apiRouter = require('./api');
module.exports = function(app) {
    app.use('/api', apiRouter);
    app.use('/api-docs', apiDocRouter);
}