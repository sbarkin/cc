'use strict';


const v1Router = require('./v1');
const authRouter = require('./auth/auth.router');
const authMiddleware = require('./auth/auth.middleware');
const express = require('express');
const router = express.Router();

router.use('/auth', authRouter);
router.use('/v1', authMiddleware);
router.use('/v1', v1Router);

module.exports = router;