'use strict';

const config = require('./../../../config/config');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
let models = require('./../../../models');
const Op = models.Sequelize.Op;
const commonService = require('./../../../lib/common.service');
const forgotPassword = require('./auth.forgot-password');
const checkResetPasswordToken = require('./auth.check-reset-password-token');
const resetPassword = require('./auth.reset-password');
const requestAccess = require('./auth.request-access');

module.exports = router;

/**
 *  @swagger
 *  /auth/patient:
 *      post:
 *          tags:
 *              - auth
 *          summary: Authenticate patient and get auth token
 *          description: Returns token if success auth.
 *          parameters:
 *              -   name: body
 *                  required: true
 *                  in: body
 *                  schema:
 *                      type: object
 *                      required:
 *                          -   name
 *                              last_four_sn
 *                      properties:
 *                          name:
 *                              type: string
 *                              example: Peter B
 *                          last_four_sn:
 *                              type: number
 *                              example: 1234
 *          responses:
 *              200:
 *                  description: User token
 *                  schema:
 *                      items:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                              token:
 *                                  type: string
 *                              userId:
 *                                  type: number
 *              400:
 *                  description: Name and last four of social are required
 *              401:
 *                  description: Authentication failed
 */
router.post('/patient', (req, res, next) => {
    if (!req.body || !req.body.name || !req.body.last_four_sn) {
        return res.status(400).json({
            success: false,
            message: 'Name and last four of social are required.'
        });
    }
    // parse patient name
    let words = req.body.name.split(' ').filter(word => word);
    const firstName = words.shift();
    const lastName = words.length ? words.pop() : '';
    models.patients.findOne({
        attributes: ['id', 'first_name', 'last_name', 'room_number'],
        where: {
            patient_actions_id: 1, //admit
            last_four_sn: req.body.last_four_sn,
            [Op.or]: [{
                    [Op.and]: [
                        { first_name: firstName },
                        { last_name: lastName }
                    ]
                },
                models.sequelize.where(
                    models.Sequelize.fn('CONCAT',
                        models.Sequelize.fn('LEFT', models.Sequelize.col('first_name'), 1),
                        ' ',
                        models.Sequelize.col('last_name')),
                    firstName[0] + ' ' + lastName),
                models.sequelize.where(
                    models.Sequelize.fn('CONCAT', models.Sequelize.col('first_name'),
                        ' ',
                        models.Sequelize.fn('LEFT', models.Sequelize.col('last_name'), 1)
                    ),
                    firstName + ' ' + (lastName ? lastName[0] : lastName))
            ]
        }
    }).then(result => {
        if (!result) {
            return res.status(401).json({
                success: false,
                message: 'Authentication failed.'
            });
        }
        const payload = {
            id: result.id,
            type: 'patient'
        };
        const token = jwt.sign(payload, config.auth.secret, {
            expiresIn: config.auth.expiresInMinutes + 'm'
        });
        res.json({
            success: true,
            token: token,
            userId: result.id
        });
    }).catch(err => {
        next(err);
    });
});

/**
 *  @swagger
 *  /auth/user:
 *      post:
 *          tags:
 *              - auth
 *          summary: Authenticate user and get auth token
 *          description: Returns token if success auth.
 *          parameters:
 *              -   name: body
 *                  required: true
 *                  in: body
 *                  schema:
 *                      type: object
 *                      required:
 *                          -   username
 *                              password
 *                      properties:
 *                          username:
 *                              type: string
 *                              example: admin@test.com
 *                          password:
 *                              type: string
 *          responses:
 *              200:
 *                  description: User token
 *                  schema:
 *                      items:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                              token:
 *                                  type: string
 *                              userId:
 *                                  type: number
 *              400:
 *                  description: Username and password are required
 *              401:
 *                  description: Authentication failed
 */
router.post('/user', (req, res, next) => {
    if (!req.body || !req.body.username || !req.body.password) {
        return res.status(400).json({
            success: false,
            message: 'Username and password are required.'
        });
    }
    // parse patient name
    let user;
    models.users.findOne({
        attributes: ['id', 'first_name', 'last_name', 'email', 'password', 'user_roles_id'],
        where: {
            user_statuses_id: 1, //active
            email: req.body.username
        }
    }).then(result => {
        if (!result) {
            res.status(401).json({
                success: false,
                message: 'Username failed.'
            });
            return Promise.reject(false);
        } else {
            user = result;
            return commonService.compareHash(req.body.password, user.password);
        }
    }).then(result => {
        if (!result) {
            return res.status(401).json({
                success: false,
                message: 'Password failed.'
            });
        }
        const payload = {
            id: user.id,
            type: 'user'
        };
        const token = jwt.sign(payload, config.auth.secret, {
            expiresIn: config.auth.expiresInMinutes + 'm'
        });
        res.json({
            success: true,
            token: token,
            userId: user.id
        });
    }).catch(err => {
        if (err) {
            next(err);
        }
    });
});

router.post('/forgot-password', forgotPassword);
router.get('/reset-password/:token/check', checkResetPasswordToken);
router.put('/reset-password/:token', resetPassword);
router.post('/request-access', requestAccess);