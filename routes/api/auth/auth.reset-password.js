'use strict';

const config = require('./../../../config/config');
let models = require('./../../../models');
const Op = models.Sequelize.Op;
const commonService = require('./../../../lib/common.service');

const path = require('path');

const hbs = require('nodemailer-express-handlebars');
const nodemailer = require('nodemailer');

const smtpTransport = nodemailer.createTransport({
    service: config.mailer.serviceProvider,
    auth: {
        user: config.mailer.email,
        pass: config.mailer.password
    }
});

const handlebarsOptions = {
    viewEngine: 'handlebars',
    viewPath: path.resolve('./routes/api/templates/'),
    extName: '.html'
};

smtpTransport.use('compile', hbs(handlebarsOptions));

/**
 *  @swagger
 *  /auth/reset-password/{token} :
 *      put:
 *          tags:
 *              - auth
 *          summary: Update password
 *          description: Update password
 *          parameters:
 *              -   in: path
 *                  name: token
 *                  required: true
 *                  description: reset password token 
 *              -   name: body
 *                  in: body
 *                  description: User data
 *                  schema:
 *                      type: object
 *                      properties:
 *                          password:
 *                              type: string
 *          responses:
 *              204:
 *                  description: Successful operation
 *              400:
 *                  description: Token and password are required
 *              401:
 *                  description: Token was expired
 */

module.exports = (req, res, next) => {
    if (!req.params.token || !req.body || !req.body.password) {
        return res.status(400).json({
            success: false,
            message: 'Token and password are required'
        });
    }

    models.users.findOne({
        where: {
            user_statuses_id: 1, //active
            reset_password_token: req.params.token,
            reset_password_expired: {
                [Op.gt]: Date.now()
            }
        }
    }).then(user => {
        if (!user) {
            return res.status(401).json({
                success: false,
                message: 'Token was expired.'
            });
        }
        return commonService.setHash(req.body.password)
            .then(hashPassword => {
                user.set('password', hashPassword);
                user.set('reset_password_token', null);
                user.set('reset_password_expired', null);
                return user.save();
            }).then(result => {
                res.status(204).send();
                if (user.user_roles_id !== 1) {
                    return sendNewPasswordUser(user, req.body.password);
                }
            });
    }).catch(err => {
        console.log('error:', err);
        if (err) {
            next(err);
        }
    });
};

function sendNewPasswordUser(user, password) {
    const data = {
        to: user.email,
        from: config.mailer.from + ' <' + config.mailer.email + '>',
        template: 'new-password-email-user',
        subject: 'Your New Password',
        context: {
            url: config.webApp.baseHref + '/login',
            password: password,
            assetsUrl: config.mailer.assetsUrl
        }
    };

    return smtpTransport.sendMail(data);
}