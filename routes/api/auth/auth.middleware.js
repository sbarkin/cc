'use strict';

const config = require('./../../../config/config');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
let models = require('./../../../models');
const Op = models.Sequelize.Op;

module.exports = router;

router.use((req, res, next) => {
    const token = req.headers['x-access-token'];
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }
    jwt.verify(token, config.auth.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });
        }
        // if everything good, save to request for use in other routes
        req.userId = decoded.id;
        req.userType = decoded.type;
        // check user roles and status
        if (req.userType === 'user') {
            models.users.findById(req.userId)
                .then(user => {
                    if (!user || user.user_statuses_id !== 1) {
                        res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });
                    } else {
                        req.user_roles_id = user.user_roles_id;
                        next();
                    }
                }).catch(err => {
                    next(err);
                });
        } else {
            models.patients.findById(req.userId)
                .then(patient => {
                    if (!patient || patient.patient_actions_id !== 1) {
                        res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });
                    } else {
                        next();
                    }
                }).catch(err => {
                    next(err);
                });
        }

    });
});