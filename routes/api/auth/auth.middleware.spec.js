'use strict';

const request = require('supertest');
let api = require('./../../../lib/apiServer');

let tokenMock = getToken({ id: 'userId' });
/**
 * @test {authMiddleware}
 */
describe('Auth middleware', () => {
    it('should return message Hello!', async() => {
        const response = await request(api.app)
            .get('/api/v1/test')
            .set('x-access-token', tokenMock)
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body).to.eqls({
            message: "Hello!"
        })
    });
    it('should return 401 when token do not set', async() => {
        const response = await request(api.app)
            .get('/api/v1/test')
            .set('Accept', 'application/json')
            .expect(401);
        console.log(response.body);
        expect(response.body).to.eqls({
            'auth': false,
            'message': 'No token provided.'
        });
    });
    it('should return 401 when token wrong', async() => {
        const tokenWrong = 'tokenWrong';
        const response = await request(api.app)
            .get('/api/v1/test')
            .set('x-access-token', tokenWrong)
            .set('Accept', 'application/json')
            .expect(401);
        console.log(response.body);
        expect(response.body).to.eqls({
            'auth': false,
            'message': 'Failed to authenticate token.'
        });
    });
});