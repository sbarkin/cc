'use strict';

const config = require('./../../../config/config');
let models = require('./../../../models');
const Op = models.Sequelize.Op;

const path = require('path');

const hbs = require('nodemailer-express-handlebars');
const nodemailer = require('nodemailer');

const smtpTransport = nodemailer.createTransport({
    service: config.mailer.serviceProvider,
    auth: {
        user: config.mailer.email,
        pass: config.mailer.password
    }
});

const handlebarsOptions = {
    viewEngine: 'handlebars',
    viewPath: path.resolve('./routes/api/templates/'),
    extName: '.html'
};

smtpTransport.use('compile', hbs(handlebarsOptions));

/**
 *  @swagger
 *  /auth/request-access:
 *      post:
 *          tags:
 *              - auth
 *          summary: Submit request access
 *          description: Submit request access
 *          parameters:
 *              -   name: body
 *                  required: true
 *                  in: body
 *                  schema:
 *                      type: object
 *                      required:
 *                          -   name
 *                          -   email
 *                      properties:
 *                          name:
 *                              type: string
 *                          email:
 *                              type: string
 *          responses:
 *              200:
 *                  description: request result
 *                  schema:
 *                      items:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *              400:
 *                  description: Name and email are required
 */

module.exports = (req, res, next) => {
    if (!req.body || !req.body.name || !req.body.email) {
        return res.status(400).json({
            success: false,
            message: 'Name and email are required.'
        });
    }

    models.users.findAll({
        where: {
            user_roles_id: 1
        }
    }).then(result => {
        result.forEach(admin => {
            sendRequestAccess(req.body.name, req.body.email, admin).catch(err => {
                console.log('error send user request access:', err);
            });
        });
        res.json({
            success: true,
        });
    }).catch(err => {
        console.log('error:', err);
        if (err) {
            next(err);
        }
    });
}

function sendRequestAccess(name, email, admin) {
    const data = {
        to: admin.email,
        from: config.mailer.from + ' <' + config.mailer.email + '>',
        template: 'request-access-email',
        subject: 'New User Request',
        context: {
            name,
            email,
            url: config.webApp.baseHref + '/users/staff',
            assetsUrl: config.mailer.assetsUrl
        }
    };

    return smtpTransport.sendMail(data);
}