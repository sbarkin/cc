'use strict';
const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');

const authRouter = require('./auth.router');

const app = express();
app.use(bodyParser.json());
app.use('/api/auth', authRouter);

/**
 * @test {authRouter}
 */
describe('POST /api/auth/patient methods', () => {
    before(async() => {
        await initDb();
    });

    it('should return jwt token for Full name', async() => {
        const response = await request(app)
            .post('/api/auth/patient')
            .send({
                'name': 'Firstname Lastname',
                'last_four_sn': 1234
            })
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.success).to.be.true;
        expect(response.body.token).to.not.be.empty;
        expect(response.body.userId).to.equal(1);
    });

    it('should return jwt token for Last four + full first name and initial of last name', async() => {
        const response = await request(app)
            .post('/api/auth/patient')
            .send({
                'name': 'Firstname L',
                'last_four_sn': 1234
            })
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.success).to.be.true;
        expect(response.body.token).to.not.be.empty;
        expect(response.body.userId).to.equal(1);
    });

    it('should return jwt token for Last four + first initial and full last name', async() => {
        const response = await request(app)
            .post('/api/auth/patient')
            .send({
                'name': 'F Lastname',
                'last_four_sn': 1234
            })
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.success).to.be.true;
        expect(response.body.token).to.not.be.empty;
        expect(response.body.userId).to.equal(1);
    });

    it('should return jwt token for spelling wrong first name but first initial and last name is correct', async() => {
        const response = await request(app)
            .post('/api/auth/patient')
            .send({
                'name': 'Firch Lastname',
                'last_four_sn': 1234
            })
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.success).to.be.true;
        expect(response.body.token).to.not.be.empty;
        expect(response.body.userId).to.equal(1);
    });

    it('should return jwt token for spelling wrong last name but first name and first initial of last name is correct', async() => {
        const response = await request(app)
            .post('/api/auth/patient')
            .send({
                'name': 'Firstname Las',
                'last_four_sn': 1234
            })
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.success).to.be.true;
        expect(response.body.token).to.not.be.empty;
        expect(response.body.userId).to.equal(1);
    });

    it('should return jwt token for right first and last name or cases above without middle name', async() => {
        const response = await request(app)
            .post('/api/auth/patient')
            .send({
                'name': 'Firstname Middlename Las',
                'last_four_sn': 1234
            })
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.success).to.be.true;
        expect(response.body.token).to.not.be.empty;
        expect(response.body.userId).to.equal(1);
    });

    it('should return status 400 for empty params', async() => {
        const response = await request(app)
            .post('/api/auth/patient')
            .send({})
            .set('Accept', 'application/json')
            .expect(400);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('Name and last four of social are required.');
    });

    it('should return status 401 when wrong params', async() => {
        const response = await request(app)
            .post('/api/auth/patient')
            .send({
                'name': 'Lastname',
                'last_four_sn': 1234
            })
            .set('Accept', 'application/json')
            .expect(401);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('Authentication failed.');
    });
});

describe('POST /api/auth/user methods', () => {
    before(async() => {
        await initDb();
    });

    it('should return jwt token for username and password', async() => {
        const response = await request(app)
            .post('/api/auth/user')
            .send({
                'username': 'test@test.com',
                'password': '123456'
            })
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.success).to.be.true;
        expect(response.body.token).to.not.be.empty;
        expect(response.body.userId).to.equal(1);
    });


    it('should return status 400 for empty params', async() => {
        const response = await request(app)
            .post('/api/auth/user')
            .send({})
            .set('Accept', 'application/json')
            .expect(400);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('Username and password are required.');
    });

    it('should return status 401 when wrong username', async() => {
        const response = await request(app)
            .post('/api/auth/user')
            .send({
                'username': 'wrongname',
                'password': '123456'
            })
            .set('Accept', 'application/json')
            .expect(401);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('Username failed.');
    });

    it('should return status 401 when wrong password', async() => {
        const response = await request(app)
            .post('/api/auth/user')
            .send({
                'username': 'test@test.com',
                'password': 'wrongpassword'
            })
            .set('Accept', 'application/json')
            .expect(401);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('Password failed.');
    });
});