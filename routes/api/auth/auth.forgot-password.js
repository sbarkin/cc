'use strict';

const config = require('./../../../config/config');
let models = require('./../../../models');
const Op = models.Sequelize.Op;

const crypto = require('crypto');
const path = require('path');

const hbs = require('nodemailer-express-handlebars');
const nodemailer = require('nodemailer');

const smtpTransport = nodemailer.createTransport({
    service: config.mailer.serviceProvider,
    auth: {
        user: config.mailer.email,
        pass: config.mailer.password
    }
});

const handlebarsOptions = {
    viewEngine: 'handlebars',
    viewPath: path.resolve('./routes/api/templates/'),
    extName: '.html'
};

smtpTransport.use('compile', hbs(handlebarsOptions));

/**
 *  @swagger
 *  /auth/forgot-password:
 *      post:
 *          tags:
 *              - auth
 *          summary: Forgot password request by user
 *          description: Forgot password request by user
 *          parameters:
 *              -   name: body
 *                  required: true
 *                  in: body
 *                  schema:
 *                      type: object
 *                      required:
 *                          -   username
 *                      properties:
 *                          username:
 *                              type: string
 *                              example: admin@test.com
 *          responses:
 *              200:
 *                  description: request result
 *                  schema:
 *                      items:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                              result:
 *                                  type: string
 *                              message:
 *                                  type: string
 *              400:
 *                  description: Username is required
 *              401:
 *                  description: Username failed
 */

module.exports = (req, res, next) => {
    if (!req.body || !req.body.username) {
        return res.status(400).json({
            success: false,
            message: 'Username is required.'
        });
    }
    let user;
    let resetToken;
    const resetTokenLength = 20;
    models.users.findOne({
        attributes: ['id', 'first_name', 'last_name', 'email', 'password', 'user_roles_id'],
        where: {
            user_statuses_id: 1, //active
            email: req.body.username
        }
    }).then(result => {
        if (!result) {
            res.status(401).json({
                success: false,
                message: 'Username failed.'
            });
            return Promise.reject(false);
        }

        user = result;
        // Create reset token
        return new Promise((resolve, reject) => {
            crypto.randomBytes(resetTokenLength, (err, buf) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(buf.toString('hex'));
                }
            });
        });

    }).then(result => {
        resetToken = result;
        user.set('reset_password_token', resetToken);
        user.set('reset_password_expired', Date.now() + 86400000);
        return user.save();
    }).then(result => {
        if (user.user_roles_id === 1) {
            return sendResetPasswordAdmin(user, resetToken);
        } else {
            return models.users.findAll({
                where: {
                    user_roles_id: 1
                }
            }).then(result => {
                result.forEach(admin => {
                    sendResetPasswordUser(user, admin, resetToken).catch(err => {
                        console.log('error send user reset password request:', err);
                    });
                });
                return true;
            });
        }

    }).then(result => {
        if (user.user_roles_id === 1) {
            res.json({
                success: true,
                result: 'Password reset link sent!',
                message: 'Please check your email for reset password link.'
            });
        } else {
            res.json({
                success: true,
                result: 'Request Submitted!',
                message: 'Your reset password request has been submitted to the system admin.'
            });
        }
    }).catch(err => {
        console.log('error:', err);
        if (err) {
            next(err);
        }
    });
};

function sendResetPasswordAdmin(user, resetToken) {
    const data = {
        to: user.email,
        from: config.mailer.from + ' <' + config.mailer.email + '>',
        template: 'forgot-password-email-admin',
        subject: 'New Password',
        context: {
            url: config.webApp.baseHref + '/reset-password/' + resetToken,
            assetsUrl: config.mailer.assetsUrl
        }
    };

    return smtpTransport.sendMail(data);
}

function sendResetPasswordUser(user, admin, resetToken) {
    const data = {
        to: admin.email,
        from: config.mailer.from + ' <' + config.mailer.email + '>',
        template: 'forgot-password-email-user',
        subject: 'Employee Reset Password Request',
        context: {
            url: config.webApp.baseHref + '/reset-password/' + resetToken,
            username: user.first_name + ' ' + user.last_name,
            assetsUrl: config.mailer.assetsUrl
        }
    };

    return smtpTransport.sendMail(data);
}