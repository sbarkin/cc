'use strict';

const config = require('./../../../config/config');
let models = require('./../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /auth/reset-password/{token}/check :
 *      get:
 *          tags:
 *              - auth
 *          summary: Check reset password token
 *          description: Check reset password token
 *          parameters:
 *              -   in: path
 *                  name: token
 *                  required: true
 *                  description: reset password token 
 *          responses:
 *              200:
 *                  description: check result
 *                  schema:
 *                      items:
 *                          type: object
 *                          properties:
 *                              success:
 *                                  type: boolean
 *                              user_roles_id:
 *                                  type: number
 *              400:
 *                  description: Token is required
 *              401:
 *                  description: Token was expired
 */

module.exports = (req, res, next) => {
    if (!req.params.token) {
        return res.status(400).json({
            success: false,
            message: 'Token is required.'
        });
    }

    models.users.findOne({
        attributes: [
            'id', [models.Sequelize.fn('CONCAT', models.Sequelize.col('first_name'), ' ', models.Sequelize.col('last_name')), 'user_name'],
            'user_roles_id',
            'reset_password_token'
        ],
        where: {
            user_statuses_id: 1, //active
            reset_password_token: req.params.token,
            reset_password_expired: {
                [Op.gt]: Date.now()
            }
        }
    }).then(result => {
        if (!result) {
            res.status(401).json({
                success: false,
                message: 'Token was expired.'
            });
        } else {
            res.json(result);
        }

    }).catch(err => {
        console.log('error:', err);
        if (err) {
            next(err);
        }
    });
};