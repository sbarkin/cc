'use strict';
const request = require('supertest');
let api = require('./../../../lib/apiServer');

let tokenMock = getToken({ id: 1, type: 'patient' });

/**
 * @test {callTypesRouter}
 */
describe('POST /api/v1/call-types methods', () => {
    before(async() => {
        await initDb();
    });
    it('should return call types', async() => {
        const response = await request(api.app)
            .get('/api/v1/call-types')
            .set('x-access-token', tokenMock)
            .set('Accept', 'application/json')
            .expect(200);
        let callTypes = await models.call_types.findAll({ raw: true });
        expect(response.body).to.eqls(callTypes);
    });
});