'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/patients:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - patients
 *          summary: Get list of patients
 *          description: Return list of patients
 *          parameters:
 *              -   in: query
 *                  name: state
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - all
 *                          - active
 *                  required: true
 *                  description: state of patient
 *              -   in: query
 *                  name: sort-by
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - patient
 *                          - room
 *                          - lastFourSn
 *                          - action
 *                  description: patient sort by
 *              -   in: query
 *                  name: sort-order
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - asc
 *                          - desc
 *                  description: patient sort order 
 *              -   in: query
 *                  name: q
 *                  shema:
 *                      type: string
 *                  description: searching
 *              -   in: query
 *                  name: offset
 *                  schema:
 *                      type: integer
 *                  description: The number of items to skip before starting to collect the result set
 *              -   in: query
 *                  name: limit
 *                  schema:
 *                      type: integer
 *                  description: The numbers of items to return             
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                          result:
 *                              $ref: '#/definitions/Patients'
 *              400:
 *                  description: State is required 
 *              403:
 *                  description: You not have the permissions for this action 
 */
module.exports = (req, res, next) => {
    if (!req.query || !req.query.state) {
        return res.status(400).json({
            success: false,
            message: 'State is required.'
        });
    }
    const state = req.query.state;
    if (state !== 'all' && state !== 'active') {
        return res.status(400).json({
            success: false,
            message: 'State is wrong. Shoud be all or active'
        });
    }
    if (req.userType !== 'user') {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const limit = req.query.limit ? parseInt(req.query.limit) : 20;
    const offset = req.query.offset ? parseInt(req.query.offset) : 0;
    const where = {};
    if (req.query.q) {
        const searchText = req.query.q;
        where[Op.or] = [
            models.sequelize.where(models.Sequelize.fn('CONCAT', models.Sequelize.col('first_name'), ' ', models.Sequelize.col('last_name')), {
                [Op.like]: `%${searchText}%`
            }),
            models.sequelize.where(models.Sequelize.literal('room_number'), {
                [Op.like]: `%${searchText}%`
            }),
            models.sequelize.where(models.Sequelize.literal('last_four_sn'), {
                [Op.like]: `%${searchText}%`
            }),
            models.sequelize.where(models.Sequelize.literal('patient_action.name'), {
                [Op.like]: `%${searchText}%`
            })
        ];
    }

    let order = [];
    if (req.query['sort-by']) {
        let sortOrder = (req.query['sort-order'] ? req.query['sort-order'] : 'asc');
        switch (req.query['sort-by']) {
            case 'room':
                order = [
                    [models.Sequelize.col("room_number"), sortOrder]
                ];
                break;
            case 'patient':
                order = [
                    [models.Sequelize.fn('CONCAT', models.Sequelize.col('first_name'), ' ', models.Sequelize.col('last_name')), sortOrder]
                ];
                break;
            case 'lastFourSn':
                order = [
                    [models.Sequelize.col("last_four_sn"), sortOrder]
                ];
                break;
            case 'action':
                order = [
                    [models.Sequelize.col("patient_action.name"), sortOrder]
                ];
                break;
        }
    }
    if (state === 'active') {
        where.patient_actions_id = 1;
    } else {
        where.patient_actions_id = {
            [Op.in]: [1, 2]
        };
    }
    models.patients.findAndCountAll({
        attributes: [
            'id', [models.Sequelize.fn('CONCAT', models.Sequelize.col('first_name'), ' ', models.Sequelize.col('last_name')), 'patient_name'],
            'first_name',
            'last_name',
            'last_four_sn',
            'room_number'
        ],
        include: [{
            model: models.patient_actions
        }],
        where,
        order,
        limit,
        offset
    }).then(result => {
        res.send({
            success: true,
            result: result
        });
    }).catch(err => {
        next(err);
    });
}