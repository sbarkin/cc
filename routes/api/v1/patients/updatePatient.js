'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/patients/{patientId}:
 *      put:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - patients
 *          summary: update Patient
 *          description: update patient
 *          parameters:
 *              -   in: path
 *                  name: patientId
 *                  required: true
 *                  description: patient id
 *              -   name: body
 *                  in: body
 *                  description: Patient data
 *                  schema:
 *                      type: object
 *                      properties:
 *                          first_name:
 *                              type: string
 *                          last_name: 
 *                              type: string
 *                          room_number:
 *                              type: string
 *                          last_four_sn:
 *                              type: string
 *                              pattern: '^\d{4}$'
 *                          patient_actions_id:
 *                              type: number
 *                              enum: [1, 2, 3]
 *          responses:
 *              204:
 *                  description: Successful operation
 *              400:
 *                  description: Patient id is required.
 *              403:
 *                  description: You not have the permissions for this action
 *              404:
 *                  description: Patient not found 
 */
module.exports = (req, res, next) => {
    if (!req.params || !req.params.patientId) {
        return res.status(400).json({
            success: false,
            message: 'Patient id is required.'
        });
    }
    let patientId = req.params.patientId;
    let actionId = req.body.patient_actions_id;

    if (actionId && [1, 2, 3].indexOf(actionId) === -1) {
        return res.status(400).json({
            success: false,
            message: 'Action id should be 1, 2 or 3.'
        });
    }

    if (req.userType !== 'user' || req.user_roles_id !== 1) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const updateFields = ['first_name', 'last_name', 'room_number', 'last_four_sn', 'patient_actions_id'];
    models.patients.findOne({
        where: {
            id: patientId
        }
    }).then(patient => {
        if (!patient) {
            return res.status(404).json({
                success: false,
                message: 'Patient not found.'
            });
        } else {
            updateFields.forEach(field => {
                if (field in req.body) {
                    patient.set(field, req.body[field]);
                }
            });
            return patient.save().then(result => {
                if (actionId === 2) {
                    return resolvePatientOpenCalls(patientId, req.userId);
                } else {
                    return true;
                }
            }).then(() => {
                global.socketServer.sendMessageToUsers('updatePatients', patient.get({ plain: true }));
                global.socketServer.sendMessagePatientid(patientId, 'updatePatient', patient.get({ plain: true }));
                return res.status(204).send();
            });
        }
    }).catch(err => {
        next(err);
    });
}

function resolvePatientOpenCalls(patientId, userId) {
    const note = 'patient has been discharged';
    const actionId = 4;
    return models.calls.update({
        call_actions_id: actionId,
        action_by_users_id: userId,
        note
    }, {
        where: {
            patients_id: patientId,
            call_actions_id: 1
        }
    }).then(() => {
        global.socketServer.sendMessageToUsers('updateCall');
        return true;
    });
}