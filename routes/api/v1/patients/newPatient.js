'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/patients:
 *      post:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - patients
 *          summary: Add new patient
 *          description: Return patient id
 *          parameters:
 *              -   name: body
 *                  required: true
 *                  description: Patient object
 *                  in: body
 *                  schema:
 *                      type: object
 *                      required:
 *                          - first_name
 *                          - last_name
 *                          - room_number
 *                          - last_four_sn
 *                      properties:
 *                          first_name:
 *                              type: string
 *                          last_name: 
 *                              type: string
 *                          room_number:
 *                              type: string
 *                          last_four_sn:
 *                              type: string
 *                              pattern: '^\d{4}$'
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success: 
 *                              type: boolean   
 *                          id:
 *                              type: number
 *              400:
 *                  description: first_name, last_name, room_number, last_four_sn are required 
 *              403:
 *                  description: You not have the permissions for this action
 */
module.exports = (req, res, next) => {
    if (!req.body || !req.body.first_name || !req.body.last_name || !req.body.room_number || !req.body.last_four_sn) {
        return res.status(400).json({
            success: false,
            message: 'first_name, last_name, room_number, last_four_sn are required.'
        });
    }
    if (req.userType !== 'user' || req.user_roles_id !== 1) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const units_id = 1;
    const patient_actions_id = 1
    models.patients.create({
        units_id,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        last_four_sn: req.body.last_four_sn,
        room_number: req.body.room_number,
        patient_actions_id
    }).then(result => {
        global.socketServer.sendMessageToUsers('updatePatients');
        res.send({
            success: true,
            id: result.id
        });
    }).catch(err => {
        next(err);
    });
}