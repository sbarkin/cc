'use strict';

const express = require('express');
const router = express.Router();

const getPatients = require('./getPatients');
const newPatient = require('./newPatient');
const updatePatient = require('./updatePatient');
const getTotalCalls = require('./getTotalCalls');

module.exports = router;

router.get('/', getPatients);
router.post('/', newPatient);
router.put('/:patientId', updatePatient);
router.get('/:patientId/total-calls', getTotalCalls);