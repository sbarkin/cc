'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/patients/{patientId}/total-calls:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - patients
 *          summary: Get total calls for patient
 *          description: Return total calls for patient
 *          parameters:
 *              -   in: path
 *                  name: patientId
 *                  required: true
 *                  description: patient id
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                          total:
 *                              type: number
 *              400:
 *                  description: Patient id is required 
 *              403:
 *                  description: You not have the permissions for this action 
 */
module.exports = (req, res, next) => {
    if (!req.params || !req.params.patientId) {
        return res.status(400).json({
            success: false,
            message: 'Patient id is required.'
        });
    }
    if (req.userType !== 'user') {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    models.calls.findAll({
        attributes: [
            [models.Sequelize.fn('count', models.Sequelize.col('id')), 'total']
        ],
        where: {
            patients_id: req.params.patientId
        },
    }).then(result => {
        res.send({
            success: true,
            result: result
        });
    }).catch(err => {
        next(err);
    });
}