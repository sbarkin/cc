'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/calls:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - calls
 *          summary: Get list of calls
 *          description: Return list of calls
 *          parameters:
 *              -   in: query
 *                  name: state
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - open
 *                          - resolved
 *                  required: true
 *                  description: state of call
 *              -   in: query
 *                  name: type
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - pain
 *                          - bathroom
 *                          - drink
 *                          - tvremote
 *                          - question 
 *                  description: type of call
 *              -   in: query
 *                  name: sort-by
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - id
 *                          - timelapsed
 *                          - room
 *                          - patient
 *                          - calltype
 *                          - timereceived 
 *                  description: sort by call
 *              -   in: query
 *                  name: sort-order
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - asc
 *                          - desc
 *                  description: sort order call 
 *              -   in: query
 *                  name: q
 *                  shema:
 *                      type: string
 *                  description: searching
 *              -   in: query
 *                  name: offset
 *                  schema:
 *                      type: integer
 *                  description: The number of items to skip before starting to collect the result set
 *              -   in: query
 *                  name: limit
 *                  schema:
 *                      type: integer
 *                  description: The numbers of items to return             
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                          result:
 *                              $ref: '#/definitions/Calls'
 *              400:
 *                  description: State is required 
 *              403:
 *                  description: You not have the permissions for this action 
 */
module.exports = (req, res, next) => {
    if (!req.query || !req.query.state) {
        return res.status(400).json({
            success: false,
            message: 'State is required.'
        });
    }
    const state = req.query.state;
    if (state !== 'open' && state !== 'resolved') {
        return res.status(400).json({
            success: false,
            message: 'State is wrong. Shoud be open or resolved'
        });
    }
    if (req.userType !== 'user') {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const limit = req.query.limit ? parseInt(req.query.limit) : 20;
    const offset = req.query.offset ? parseInt(req.query.offset) : 0;
    const where = {};
    where[Op.or] = [];
    where[Op.and] = [];
    if (req.query.type) {
        const callTypes = req.query.type.split(',');
        if (callTypes.lenght === 1) {
            where.call_types_id = getTypeId(req.query.type);
        } else {
            const whereCallType = [];
            callTypes.forEach(item => {
                whereCallType.push(
                    models.sequelize.where(models.Sequelize.col('call_types_id'), getTypeId(item))
                );
            });
            where[Op.and].push({
                [Op.or]: whereCallType
            });
        }
    }

    if (req.query.range) {
        switch (req.query.range) {
            case '1 Week':
                where[Op.and] = where[Op.and].concat([
                    models.sequelize.where(models.Sequelize.fn('TIMESTAMPDIFF', models.Sequelize.literal('WEEK'), models.Sequelize.col('date'), models.Sequelize.fn('Now')), {
                        [Op.lte]: 1
                    })
                ]);
                break;
            case '4 Weeks':
                where[Op.and] = where[Op.and].concat([
                    models.sequelize.where(models.Sequelize.fn('TIMESTAMPDIFF', models.Sequelize.literal('WEEK'), models.Sequelize.col('date'), models.Sequelize.fn('Now')), {
                        [Op.lte]: 4
                    })
                ]);
                break;
            case '1 Year':
                where[Op.and] = where[Op.and].concat([
                    models.sequelize.where(models.Sequelize.fn('TIMESTAMPDIFF', models.Sequelize.literal('YEAR'), models.Sequelize.col('date'), models.Sequelize.fn('Now')), {
                        [Op.lte]: 1
                    })
                ]);
                break;
            case 'Mtd':
                where.date = {
                    [Op.gte]: models.Sequelize.fn('DATE_FORMAT', models.Sequelize.fn('Now'), '%Y-%m-01')
                };
                break;
            case 'Ytd':
                where.date = {
                    [Op.gte]: models.Sequelize.fn('DATE_FORMAT', models.Sequelize.fn('Now'), '%Y-01-01')
                };
                break;
        }
    }
    if (req.query.q) {
        const searchText = req.query.q;
        if (state === 'open') {
            where[Op.and].push({
                [Op.or]: [
                    models.sequelize.where(models.Sequelize.literal('calls.id'), searchText),
                    models.sequelize.where(models.Sequelize.literal('patient.room_number'), searchText),
                    models.sequelize.where(models.Sequelize.literal('call_type.title'), {
                        [Op.like]: `%${searchText}%`
                    }),
                    models.sequelize.where(models.Sequelize.fn('CONCAT', models.Sequelize.col('patient.first_name'), ' ', models.Sequelize.col('patient.last_name')), {
                        [Op.like]: `%${searchText}%`
                    }),
                    models.sequelize.where(models.Sequelize.fn('DATE_FORMAT', models.Sequelize.col('date'), '%h:%i%p'), {
                        [Op.like]: `%${searchText}%`
                    }),
                ]
            });
        } else {
            where[Op.and].push({
                [Op.or]: [
                    models.sequelize.where(models.Sequelize.literal('calls.id'), searchText),
                    models.sequelize.where(models.Sequelize.col('patient.room_number'), searchText),
                    models.sequelize.where(models.Sequelize.col('call_type.title'), {
                        [Op.like]: `%${searchText}%`
                    }),
                    models.sequelize.where(models.Sequelize.fn('CONCAT', models.Sequelize.col('patient.first_name'), ' ', models.Sequelize.col('patient.last_name')), {
                        [Op.like]: `%${searchText}%`
                    }),
                    models.sequelize.where(models.Sequelize.fn('DATE_FORMAT', models.Sequelize.col('date'), '%m/%d/%Y %h:%i%p'), {
                        [Op.like]: `%${searchText}%`
                    }),
                    models.sequelize.where(models.Sequelize.col('call_action.name'), {
                        [Op.like]: `%${searchText}%`
                    }),
                    models.sequelize.where(models.Sequelize.fn('CONCAT', models.Sequelize.col('user.first_name'), ' ', models.Sequelize.col('user.last_name')), {
                        [Op.like]: `%${searchText}%`
                    }),
                    models.sequelize.where(models.Sequelize.col('note'), {
                        [Op.like]: `%${searchText}%`
                    }),
                ]
            });
        }
    }

    if (req.query['q-staff']) {
        where[Op.and] = where[Op.and].concat([
            models.sequelize.where(models.Sequelize.fn('CONCAT', models.Sequelize.col('user.first_name'), ' ', models.Sequelize.col('user.last_name')), {
                [Op.like]: `%${req.query['q-staff']}%`
            }),
        ]);
    }

    let order = [];
    if (req.query['sort-by']) {
        let sortOrder = (req.query['sort-order'] ? req.query['sort-order'] : 'asc');
        switch (req.query['sort-by']) {
            case 'id':
                order = [
                    [models.Sequelize.col("calls.id"), sortOrder]
                ];
                break;
            case 'timelapsed':
                sortOrder = sortOrder === 'asc' ? 'desc' : 'asc';
                order = [
                    ['date', sortOrder]
                ];
                break;
            case 'room':
                order = [
                    [models.Sequelize.col("patient.room_number"), sortOrder]
                ];
                break;
            case 'patient':
                order = [
                    [models.Sequelize.fn('CONCAT', models.Sequelize.col('patient.first_name'), ' ', models.Sequelize.col('patient.last_name')), sortOrder]
                ];
                break;
            case 'calltype':
                order = [
                    [models.Sequelize.col("call_type.title"), sortOrder]
                ];
                break;
            case 'callaction':
                order = [
                    [models.Sequelize.col("call_action.name"), sortOrder]
                ];
                break;
            case 'timereceived':
                order = [
                    ['date', sortOrder]
                ];
                break;
            case 'responsetime':
                order = [
                    [models.Sequelize.fn('TIMESTAMPDIFF', models.Sequelize.literal('MINUTE'), models.Sequelize.col('date'), models.Sequelize.col('action_date')), sortOrder]
                ];
                break;
            case 'resolvedby':
                order = [
                    [models.Sequelize.fn('CONCAT', models.Sequelize.col('user.first_name'), ' ', models.Sequelize.col('user.last_name')), sortOrder]
                ];
                break;
            case 'note':
                order = [
                    ['note', sortOrder]
                ];
                break;
        }
    }
    if (state === 'open') {
        where.call_actions_id = 1;
    } else {
        where.call_actions_id = {
            [Op.in]: [2, 3, 4]
        };
    }

    if (where[Op.and].length === 0) {
        delete where[Op.and];
    }
    if (where[Op.or].length === 0) {
        delete where[Op.or];
    }
    models.calls.findAndCountAll({
        attributes: ['id', [models.Sequelize.fn('UNIX_TIMESTAMP', models.Sequelize.col('date')), 'date'],
            'patients_id', 'call_types_id', 'call_actions_id', 'note', 'action_by_users_id', [models.Sequelize.fn('UNIX_TIMESTAMP', models.Sequelize.col('action_date')), 'action_date'],
            [models.Sequelize.fn('TIMESTAMPDIFF', models.Sequelize.literal('MINUTE'), models.Sequelize.col('date'), models.Sequelize.col('action_date')), 'response_time']
        ],
        include: [{
                model: models.patients,
                attributes: ['room_number', [models.Sequelize.fn('CONCAT', models.Sequelize.col('patient.first_name'), ' ', models.Sequelize.col('patient.last_name')), 'patient_name']]
            },
            {
                model: models.call_types
            },
            {
                model: models.users,
                attributes: [
                    [models.Sequelize.fn('CONCAT', models.Sequelize.col('user.first_name'), ' ', models.Sequelize.col('user.last_name')), 'user_name']
                ]
            },
            {
                model: models.call_actions
            },
        ],
        where,
        order,
        limit,
        offset
    }).then(result => {
        res.send({
            success: true,
            result: result
        });
    }).catch(err => {
        next(err);
    });
}

function getTypeId(type) {
    let typeId = null;
    switch (type) {
        case 'pain':
            typeId = 1;
            break;
        case 'bathroom':
            typeId = 2;
            break;
        case 'drink':
            typeId = 3;
            break;
        case 'tvremote':
            typeId = 4;
            break;
        case 'question':
            typeId = 5;
            break;
    }
    return typeId;
}