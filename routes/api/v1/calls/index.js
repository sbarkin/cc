'use strict';

const express = require('express');
const router = express.Router();

const getTotalCalls = require('./getTotalCalls');
const getCalls = require('./getCalls');
const setAction = require('./setAction');

module.exports = router;

router.get('/', getCalls);
router.get('/total', getTotalCalls);
router.put('/:callId/action/:actionId', setAction);