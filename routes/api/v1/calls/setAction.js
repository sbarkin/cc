'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/calls/{callId}/action/{actionId}:
 *      put:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - calls
 *          summary: set action to call
 *          description: set action to call
 *          parameters:
 *              -   in: path
 *                  name: callId
 *                  required: true
 *                  description: call id
 *              -   in: path
 *                  name: actionId
 *                  required: true
 *                  description: action id
 *                  schema: 
 *                      type: number
 *                      enum: [2, 3, 4]
 *              -   name: body
 *                  in: body
 *                  description: note for action other
 *                  schema:
 *                      type: object
 *                      properties:
 *                          note:
 *                              type: string
 *          responses:
 *              204:
 *                  description: Successful operation
 *              400:
 *                  description: Call id and action id are required.
 *              403:
 *                  description: You not have the permissions for this action
 *              404:
 *                  description: Call id not found 
 */
module.exports = (req, res, next) => {
    if (!req.params || !req.params.callId || !req.params.actionId) {
        return res.status(400).json({
            success: false,
            message: 'Call id and action id are required.'
        });
    }
    let callId = req.params.callId;
    let actionId = req.params.actionId;
    let note = req.body.note;

    if (actionId !== '2' && actionId !== '3' && actionId !== '4') {
        return res.status(400).json({
            success: false,
            message: 'Action id should be 2, 3 or 4.'
        });
    }

    if (req.userType !== 'user') {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }

    models.calls.findOne({
        where: {
            id: callId
        }
    }).then(call => {
        if (!call) {
            return res.status(404).json({
                success: false,
                message: 'Call id not found.'
            });
        } else {
            call.set('call_actions_id', actionId);
            call.set('note', note);
            call.set('action_by_users_id', req.userId);
            call.set('action_date', models.Sequelize.fn('NOW'));
            return call.save().then(result => {
                global.socketServer.sendMessageToUsers('updateCall');
                global.socketServer.sendMessagePatientid(call.patients_id, 'resolveCall', {
                    actionId: actionId,
                    note: note,
                    userId: req.userId
                });
                return res.status(204).send();
            });
        }
    }).catch(err => {
        next(err);
    });
}