'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/calls/total:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - calls
 *          summary: Get summary data for open calls
 *          description: Return object with total open calls
 *          parameters:
 *              -   in: query
 *                  name: state
 *                  shema:
 *                      state: string
 *                      enum:
 *                          - open
 *                          - resolved
 *                  required: true
 *                  description: state calls of total
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                      success:
 *                          type: boolean
 *                      result:
 *                          $ref: '#/definitions/TotalCalls'
 *              400:
 *                  description: State is required 
 *              403:
 *                  description: You not have the permissions for this action 
 * definitions:
 *   TotalCall:
 *     type: object
 *     properties:
 *       call_types_id:
 *         type: number
 *       total:
 *         type: number
 *   TotalCalls:
 *     type: array
 *     items:
 *       $ref: '#/definitions/TotalCall'
 */
module.exports = (req, res, next) => {
    if (!req.query || !req.query.state) {
        return res.status(400).json({
            success: false,
            message: 'State is required.'
        });
    }
    const state = req.query.state;
    if (state !== 'open' && state !== 'resolved') {
        return res.status(400).json({
            success: false,
            message: 'State is wrong. Shoud be open or resolved'
        });
    }
    if (req.userType !== 'user') {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const where = {};
    if (state === 'open') {
        where.call_actions_id = 1;
    } else {
        where.call_actions_id = 2;
    }
    models.calls.findAll({
        attributes: ['call_types_id', [models.Sequelize.fn('count', models.Sequelize.col('id')), 'total']],
        where,
        group: ['call_types_id']
    }).then(result => {
        res.send({
            success: true,
            result: result
        });
    }).catch(err => {
        next(err);
    });
}