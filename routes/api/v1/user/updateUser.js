'use strict';

const saveUser = require('./saveUser');

/**
 *  @swagger
 *  /v1/user/current:
 *      put:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - user
 *          summary: update current user
 *          description: update current user
 *          parameters:
 *              -   name: body
 *                  in: body
 *                  description: Current user data
 *                  schema:
 *                      type: object
 *                      properties:
 *                          first_name:
 *                              type: string
 *                          last_name: 
 *                              type: string
 *                          email:
 *                              type: string
 *                          password:
 *                              type: string
 *                          user_roles_id:
 *                              type: number 
 *                          user_statuses_id:
 *                              type: number
 *          responses:
 *              204:
 *                  description: Successful operation
 *              403:
 *                  description: You not have the permissions for this action
 *              404:
 *                  description: user not found 
 */
module.exports = (req, res, next) => {
    if (req.userType !== 'user' || !req.userId) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    saveUser(req.userId, req.body, res).catch(err => {
        next(err);
    });
}