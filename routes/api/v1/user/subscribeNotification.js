'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;
const commonService = require('./../../../../lib/common.service');

/**
 *  @swagger
 *  /v1/user/current/notification:
 *      post:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - user
 *          summary: add user subscription to notification
 *          description: add user subscription to notification
 *          parameters:
 *              -   name: body
 *                  in: body
 *                  description: PushSubscription object
 *                  schema:
 *                      type: object
 *                      properties:
 *                          endpoint:
 *                              type: string
 *                          key: 
 *                              type: object
 *                              properties:
 *                                  auth:
 *                                      type: string
 *                                  p256dh:
 *                                      type: string
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success: 
 *                              type: boolean   
 *              403:
 *                  description: You not have the permissions for this action
 *              404:
 *                  description: user not found 
 */
module.exports = (req, res, next) => {
    if (req.userType !== 'user' || !req.userId) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const expiredDuration = 24 * 30 * 60 * 60 * 1000; // 1 month
    models.user_subscriptions.findOne({
        where: {
            users_id: req.userId,
            endpoint: req.body.endpoint
        }
    }).then(sub => {
        if (sub) {
            sub.set('auth', req.body.keys.auth);
            sub.set('p256dh', req.body.keys.p256dh);
            sub.set('expired', Date.now() + expiredDuration);
            return sub.save();
        } else {
            return models.user_subscriptions.create({
                users_id: req.userId,
                endpoint: req.body.endpoint,
                auth: req.body.keys.auth,
                p256dh: req.body.keys.p256dh,
                expired: Date.now() + expiredDuration
            });
        }
    }).then(result => {
        res.send({
            success: true
        });
    }).catch(err => {
        next(err);
    });
}