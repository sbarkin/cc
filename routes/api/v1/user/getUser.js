'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/user/current:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - user
 *          summary: Get user info of current user
 *          description: Return user object
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                       $ref: '#/definitions/User'
 *              404:
 *                  description: User id not found
 *              403:
 *                  description: You not have the permissions for this action 
 */
module.exports = (req, res, next) => {
    if (req.userType !== 'user' || !req.userId) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    models.users.findOne({
        attributes: ['id', 'first_name', 'last_name', 'email', 'user_roles_id', 'user_statuses_id'],
        where: {
            id: req.userId
        }
    }).then(result => {
        res.send(result);
    }).catch(err => {
        next(err);
    });
}