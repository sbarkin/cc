'use strict';

const express = require('express');
const router = express.Router();

const getUser = require('./getUser');
const updateUser = require('./updateUser');
const subscribeNotification = require('./subscribeNotification');
const unsubscribeNotification = require('./unsubscribeNotification');
module.exports = router;

router.get('/current', getUser);
router.put('/current', updateUser);
router.post('/current/notification', subscribeNotification);
router.delete('/current/notification', unsubscribeNotification);