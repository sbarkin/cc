'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;
const commonService = require('./../../../../lib/common.service');

/**
 *  @swagger
 *  /v1/user/current/notification:
 *      delete:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - user
 *          summary: usubscribe notification
 *          description: usubscribe notification
 *          parameters:
 *              -   name: body
 *                  in: body
 *                  description: endpoint
 *                  schema:
 *                      type: object
 *                      properties:
 *                          endpoint:
 *                              type: string
 *          responses:
 *              204:
 *                  description: Successful operation  
 *              403:
 *                  description: You not have the permissions for this action
 *              404:
 *                  description: user not found 
 */
module.exports = (req, res, next) => {
    if (req.userType !== 'user' || !req.userId) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    models.user_subscriptions.destroy({
        where: {
            users_id: req.userId,
            endpoint: req.body.endpoint
        }
    }).then(result => {
        return res.status(204).send();
    }).catch(err => {
        next(err);
    });
}