'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;
const commonService = require('./../../../../lib/common.service');

/**
 * Common method save user data.
 */
module.exports = (userId, userData, res) => {
    const updateFields = ['first_name', 'last_name', 'email', 'user_roles_id', 'user_statuses_id'];
    let promise;
    let hashPassword;
    if (userData.password) {
        promise = commonService.setHash(userData.password);
    } else {
        promise = Promise.resolve();
    }
    return promise.then(hash => {
        hashPassword = hash;
        return models.users.findOne({
            where: {
                id: userId
            }
        });
    }).then(user => {
        if (!user) {
            return res.status(404).json({
                success: false,
                message: 'User not found.'
            });
        } else {
            updateFields.forEach(field => {
                if (field in userData) {
                    user.set(field, userData[field]);
                }
            });
            if (userData.password && hashPassword) {
                user.set('password', hashPassword);
            }
            return user.save().then(result => {
                global.socketServer.sendMessageToUsers('updateUsers', user.get({ plain: true }));
                global.socketServer.sendMessageUserid(userId, 'updateUser', user.get({ plain: true }));
                return res.status(204).send();
            });
        }
    });
}