'use strict';

const express = require('express');
const router = express.Router();

const getUserRoles = require('./getUserRoles');

module.exports = router;

router.get('/', getUserRoles);