'use strict';

const express = require('express');
const router = express.Router();
let models = require('./../../../../models');
const Op = models.Sequelize.Op;

module.exports = router;

/**
 *  @swagger
 *  /v1/user-roles:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - user roles
 *          summary: List user roles
 *          description: Returns list user roles.
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                       $ref: '#/definitions/UserRoles'
 *              403:
 *                  description: No token provided.
 */
router.get('/', (req, res, next) => {
    models.user_roles.findAll({
        order: [
            ['id', 'ASC']
        ]
    }).then(result => {
        res.send(result);
    }).catch(err => {
        next(err);
    });

});