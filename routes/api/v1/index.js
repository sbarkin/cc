'use strict';

const express = require('express');
const router = express.Router();

const testRouter = require('./test.router');
const callTypesRouter = require('./callTypes.router');
const patientRouter = require('./patient');
const userRouter = require('./user');
const callsRouter = require('./calls');
const patientsRouter = require('./patients');
const staffRouter = require('./staff');
const userRolesRouter = require('./user-roles');

router.use('/test', testRouter);
router.use('/call-types', callTypesRouter);
router.use('/patient', patientRouter);
router.use('/user', userRouter);
router.use('/calls', callsRouter);
router.use('/patients', patientsRouter);
router.use('/staff', staffRouter);
router.use('/user-roles', userRolesRouter);

module.exports = router;