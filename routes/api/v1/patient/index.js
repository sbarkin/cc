'use strict';

const express = require('express');
const router = express.Router();
const createCall = require('./createCall');
const getPatient = require('./getPatient');
const getLastCall = require('./getLastCall');
const cancelCall = require('./cancelCall');

module.exports = router;

router.post('/:patientId/call', createCall);
router.get('/:patientId/call/last', getLastCall);
router.delete('/:patientId/call/:callId', cancelCall);

router.get('/:patientId', getPatient);