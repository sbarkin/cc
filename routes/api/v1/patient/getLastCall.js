'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/patient/{patientId}/call/last:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - patient
 *          summary: Get last call requst for patient
 *          description: Return call request object
 *          parameters:
 *              -   in: path
 *                  name: patientId
 *                  required: true
 *                  description: patient id
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                       $ref: '#/definitions/Call'
 *              400:
 *                  description: Patient id is required 
 *              403:
 *                  description: You not have the permissions for this action 
 */
module.exports = (req, res, next) => {
    if (!req.params || !req.params.patientId) {
        return res.status(400).json({
            success: false,
            message: 'Patient id is required.'
        });
    }
    if (req.userType !== 'patient' || parseInt(req.params.patientId) !== req.userId) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    models.calls.findOne({
        where: {
            patients_id: req.params.patientId
        },
        order: [
            ['id', 'DESC']
        ]
    }).then(result => {
        res.send({
            success: true,
            result: result
        });
    }).catch(err => {
        next(err);
    });
}