'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/patient/{patientId}/call/{callId}:
 *      delete:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - patient
 *          summary: Cancel call requst by patient
 *          description: Cancel call request by patient
 *          parameters:
 *              -   in: path
 *                  name: patientId
 *                  required: true
 *                  description: patient id
 *              -   in: path
 *                  name: callId
 *                  required: true
 *                  description: call request id
 *          responses:
 *              204:
 *                  description: Successful operation
 *              400:
 *                  description: Patient id and call request id are required 
 *              403:
 *                  description: You not have the permissions for this action 
 *              404:
 *                  description: Call request for patient id and call id not found 
 *              405:
 *                  description: Call request was already changed 
 */
module.exports = (req, res, next) => {
    if (!req.params || !req.params.patientId || !req.params.callId) {
        return res.status(400).json({
            success: false,
            message: 'Patient id and call id request are required.'
        });
    }
    if (req.userType !== 'patient' || parseInt(req.params.patientId) !== req.userId) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const canceledTypeId = 3;
    models.calls.findOne({
        where: {
            id: req.params.callId,
            patients_id: req.params.patientId
        }
    }).then(call => {
        if (!call) {
            return res.status(404).json({
                success: false,
                message: 'Call request for patient id and call id not found.'
            });
        } else if (call.call_actions_id !== 1) {
            return res.status(405).json({
                success: false,
                message: 'Call request was already changed.'
            });
        } else {
            call.set('call_actions_id', canceledTypeId);
            call.set('action_date', models.Sequelize.fn('NOW'));
            return call.save().then(result => {
                global.socketServer.sendMessageToUsers('updateCall');
                return res.status(204).send();
            });
        }
    }).catch(err => {
        next(err);
    });
}