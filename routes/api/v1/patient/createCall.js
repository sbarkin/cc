'use strict';

const config = require('./../../../../config/config');

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

const webPushService = require('./../../../../lib/webPush.service');

/**
 *  @swagger
 *  /v1/patient/{patientId}/call:
 *      post:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - patient
 *          summary: Create call requst for patient
 *          description: Return call id
 *          parameters:
 *              -   in: path
 *                  name: patientId
 *                  required: true
 *                  description: patient id
 *              -   name: body
 *                  required: true
 *                  description: Call object
 *                  in: body
 *                  schema:
 *                      type: object
 *                      required:
 *                          - call_types_id
 *                      properties:
 *                          call_types_id:
 *                              type: number
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success: 
 *                              type: boolean   
 *                          id:
 *                              type: number
 *              400:
 *                  description: Call type id and patient id are required 
 *              403:
 *                  description: You not have the permissions for this action
 */
module.exports = (req, res, next) => {
    if (!req.body || !req.body.call_types_id || !req.params || !req.params.patientId) {
        return res.status(400).json({
            success: false,
            message: 'Call type id and patient id are required.'
        });
    }
    if (req.userType !== 'patient' || parseInt(req.params.patientId) !== req.userId) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    models.calls.create({
        date: models.Sequelize.fn('NOW'),
        patients_id: req.params.patientId,
        call_types_id: req.body.call_types_id,
        call_actions_id: 1,
        action_date: models.Sequelize.fn('NOW')
    }).then(result => {
        global.socketServer.sendMessageToUsers('updateCall');
        // send push notification
        models.patients.findById(req.params.patientId)
            .then(patient => {
                models.call_types.findById(req.body.call_types_id)
                    .then(type => {
                        const message = type.short_name + ' call' +
                            ' from room ' + patient.room_number;
                        webPushService.sendNotificationAllUsers('ComminuCare',
                            message,
                            config.webApp.baseHref + '/calls/open/all'
                        );
                    });
            });

        res.send({
            success: true,
            id: result.id
        });
    }).catch(err => {
        next(err);
    });
}