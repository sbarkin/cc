'use strict';

'use strict';
const request = require('supertest');
let api = require('./../../../../lib/apiServer');

let tokenMock = getToken({ id: 1, type: 'patient' });
let callRequest;

/**
 * @test {cancelCallRouter}
 */
describe('DELETE /api/v1/patient/{patientId}/call/{callId} method', () => {
    before(async() => {
        await initDb();
        callRequest = await models.calls.create({
            date: '2018-02-28T22:00:00.000Z',
            patients_id: 1,
            call_types_id: 1,
            call_actions_id: 1,
            note: null,
            action_by_users_id: null,
            action_date: '2018-02-28T22:00:00.000Z'
        });
    });

    it('should cancel call request', async() => {
        const response = await request(api.app)
            .delete('/api/v1/patient/1/call/' + callRequest.id)
            .set('x-access-token', tokenMock)
            .send()
            .set('Accept', 'application/json')
            .expect(204);
        const result = await models.calls.findById(callRequest.id);
        expect(result.call_actions_id).to.equal(3);
    });

    it('should return status 403 when patient id not equal current patient id', async() => {
        const wrongPatientId = 2;
        const response = await request(api.app)
            .delete('/api/v1/patient/' + wrongPatientId + '/call/' + callRequest.id)
            .set('x-access-token', tokenMock)
            .send({
                'call_types_id': 1
            })
            .set('Accept', 'application/json')
            .expect(403);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('You not have the permissions for this action.');
    });

    it('should return status 404 when call request for patient id and call id not found', async() => {
        const wrongCallId = 2;
        const response = await request(api.app)
            .delete('/api/v1/patient/1/call/' + wrongCallId)
            .set('x-access-token', tokenMock)
            .send()
            .set('Accept', 'application/json')
            .expect(404);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('Call request for patient id and call id not found.');
    });

    it('should return status 405 if call request was already changed', async() => {
        const response = await request(api.app)
            .delete('/api/v1/patient/1/call/' + callRequest.id)
            .set('x-access-token', tokenMock)
            .send()
            .set('Accept', 'application/json')
            .expect(405);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('Call request was already changed.');
    });

});