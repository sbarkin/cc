'use strict';
const request = require('supertest');
let api = require('./../../../../lib/apiServer');

let tokenMock = getToken({ id: 1, type: 'patient' });

/**
 * @test {getPatientRouter}
 */
describe('GET /api/v1/patient/:patientId methods', () => {
    before(async() => {
        await initDb();
    });
    it('should return patient', async() => {
        const response = await request(api.app)
            .get('/api/v1/patient/1')
            .set('x-access-token', tokenMock)
            .set('Accept', 'application/json')
            .expect(200);
        let patient = await models.patients.findOne({
            where: {
                id: 1
            },
            raw: true
        });
        expect(response.body).to.eqls(patient);
    });

    it('should check the permissions for this patient', async() => {
        const response = await request(api.app)
            .get('/api/v1/patient/2')
            .set('x-access-token', tokenMock)
            .set('Accept', 'application/json')
            .expect(403);
    });

    it('should validate patient id', async() => {
        const response = await request(api.app)
            .get('/api/v1/patient')
            .set('x-access-token', tokenMock)
            .set('Accept', 'application/json')
            .expect(404);
    });
});