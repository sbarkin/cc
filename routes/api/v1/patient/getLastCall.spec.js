'use strict';
const request = require('supertest');
let api = require('./../../../../lib/apiServer');

let tokenMock = getToken({ id: 1, type: 'patient' });
let callRequest;
/**
 * @test {getLastCallRouter}
 */
describe('GET /api/v1/patient/:patientId/call/last methods', () => {
    before(async() => {
        await initDb();
        await models.calls.create({
            date: '2018-02-28T22:00:00.000Z',
            patients_id: 1,
            call_types_id: 1,
            call_actions_id: 2,
            note: null,
            action_by_users_id: null,
            action_date: '2018-02-28T22:00:00.000Z'
        });
        callRequest = await models.calls.create({
            date: '2018-02-28T22:00:00.000Z',
            patients_id: 1,
            call_types_id: 1,
            call_actions_id: 1,
            note: null,
            action_by_users_id: null,
            action_date: '2018-02-28T22:00:00.000Z'
        });
    });
    it('should return last call for patient', async() => {
        const response = await request(api.app)
            .get('/api/v1/patient/1/call/last')
            .set('x-access-token', tokenMock)
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.result.id).to.equals(callRequest.id);
    });

    it('should check the permissions for this patient', async() => {
        const response = await request(api.app)
            .get('/api/v1/patient/2')
            .set('x-access-token', tokenMock)
            .set('Accept', 'application/json')
            .expect(403);
    });

    it('should validate patient id', async() => {
        const response = await request(api.app)
            .get('/api/v1/patient')
            .set('x-access-token', tokenMock)
            .set('Accept', 'application/json')
            .expect(404);
    });
});