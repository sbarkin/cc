'use strict';

'use strict';
const request = require('supertest');
let api = require('./../../../../lib/apiServer');

let tokenMock = getToken({ id: 1, type: 'patient' });

/**
 * @test {createCallRouter}
 */
describe('POST /api/v1/patient/{patientId}/call method', () => {
    before(async() => {
        await initDb();
    });

    it('should create call request', async() => {
        const response = await request(api.app)
            .post('/api/v1/patient/1/call')
            .set('x-access-token', tokenMock)
            .send({
                'call_types_id': 1
            })
            .set('Accept', 'application/json')
            .expect(200);
        expect(response.body.success).to.be.true;
        expect(response.body.id).to.equal(1);
    });

    it('should return status 400 when bad parameters', async() => {
        const response = await request(api.app)
            .post('/api/v1/patient/1/call')
            .set('x-access-token', tokenMock)
            .send({})
            .set('Accept', 'application/json')
            .expect(400);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('Call type id and patient id are required.');
    });

    it('should return status 403 when patient id not equal current patient id', async() => {
        const wrongPatientId = 2;
        const response = await request(api.app)
            .post('/api/v1/patient/' + wrongPatientId + '/call')
            .set('x-access-token', tokenMock)
            .send({
                'call_types_id': 1
            })
            .set('Accept', 'application/json')
            .expect(403);
        expect(response.body.success).to.be.false;
        expect(response.body.message).to.equal('You not have the permissions for this action.');
    });
});