'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/patient/{patientId}:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - patient
 *          summary: Get patient info
 *          description: Return patient object
 *          parameters:
 *              -   in: path
 *                  name: patientId
 *                  required: true
 *                  description: patient id
 *                  schema:
 *                      type: number
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                       $ref: '#/definitions/Patient'
 *              404:
 *                  description: Patient id not found
 *              403:
 *                  description: You not have the permissions for this patient id 
 */
module.exports = (req, res, next) => {
    if (req.userType !== 'patient' || parseInt(req.params.patientId) !== req.userId) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this patient id.'
        })
    }
    models.patients.findOne({
        where: {
            id: req.params.patientId
        }
    }).then(result => {
        res.send(result);
    }).catch(err => {
        next(err);
    });
}