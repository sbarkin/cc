'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/staff:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - staff
 *          summary: Get list of staff
 *          description: Return list of staff
 *          parameters:
 *              -   in: query
 *                  name: state
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - all
 *                          - active
 *                  required: true
 *                  description: state of patient
 *              -   in: query
 *                  name: role
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - administartor
 *                          - nurse
 *                  description: staff role
 *              -   in: query
 *                  name: sort-by
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - user
 *                          - role
 *                          - status
 *                  description: staff sort by
 *              -   in: query
 *                  name: sort-order
 *                  shema:
 *                      type: string
 *                      enum:
 *                          - asc
 *                          - desc
 *                  description: staff sort order 
 *              -   in: query
 *                  name: q
 *                  shema:
 *                      type: string
 *                  description: searching
 *              -   in: query
 *                  name: offset
 *                  schema:
 *                      type: integer
 *                  description: The number of items to skip before starting to collect the result set
 *              -   in: query
 *                  name: limit
 *                  schema:
 *                      type: integer
 *                  description: The numbers of items to return             
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                          result:
 *                              $ref: '#/definitions/Users'
 *              400:
 *                  description: State is required 
 *              403:
 *                  description: You not have the permissions for this action 
 */
module.exports = (req, res, next) => {
    if (!req.query || !req.query.state) {
        return res.status(400).json({
            success: false,
            message: 'State is required.'
        });
    }
    const state = req.query.state;
    if (state !== 'all' && state !== 'active') {
        return res.status(400).json({
            success: false,
            message: 'State is wrong. Shoud be all or active'
        });
    }
    if (req.userType !== 'user') {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const limit = req.query.limit ? parseInt(req.query.limit) : 20;
    const offset = req.query.offset ? parseInt(req.query.offset) : 0;
    const where = {};
    where[Op.and] = [];
    if (state === 'active') {
        where.user_statuses_id = 1;
    } else {
        where.user_statuses_id = {
            [Op.in]: [1, 2]
        };
    }

    if (req.query.role) {
        const roles = req.query.role.split(',');
        if (roles.lenght === 1) {
            where.user_roles_id = getRoleId(req.query.role);
        } else {
            const whereUserRoles = [];
            roles.forEach(item => {
                whereUserRoles.push(
                    models.sequelize.where(models.Sequelize.col('user_roles_id'), getRoleId(item))
                );
            });
            where[Op.and].push({
                [Op.or]: whereUserRoles
            });
        }
    }

    if (req.query.q) {
        const searchText = req.query.q;
        where[Op.and].push({
            [Op.or]: [
                models.sequelize.where(models.Sequelize.fn('CONCAT', models.Sequelize.col('first_name'), ' ', models.Sequelize.col('last_name')), {
                    [Op.like]: `%${searchText}%`
                }),
                models.sequelize.where(models.Sequelize.literal('user_status.name'), {
                    [Op.like]: `%${searchText}%`
                }),
                models.sequelize.where(models.Sequelize.literal('user_role.name'), {
                    [Op.like]: `%${searchText}%`
                })
            ]
        });
    }

    if (where[Op.and].length === 0) {
        delete where[Op.and];
    }

    let order = [];
    if (req.query['sort-by']) {
        let sortOrder = (req.query['sort-order'] ? req.query['sort-order'] : 'asc');
        switch (req.query['sort-by']) {
            case 'user':
                order = [
                    [models.Sequelize.fn('CONCAT', models.Sequelize.col('first_name'), ' ', models.Sequelize.col('last_name')), sortOrder]
                ];
                break;
            case 'role':
                order = [
                    [models.Sequelize.col("user_role.name"), sortOrder]
                ];
                break;
            case 'status':
                order = [
                    [models.Sequelize.col("user_status.name"), sortOrder]
                ];
                break;
        }
    }
    models.users.findAndCountAll({
        attributes: [
            'id', [models.Sequelize.fn('CONCAT', models.Sequelize.col('first_name'), ' ', models.Sequelize.col('last_name')), 'user_name'],
            'first_name',
            'last_name',
            'email',
            'user_roles_id',
            'user_statuses_id'
        ],
        include: [{
            model: models.user_statuses
        }, {
            model: models.user_roles
        }],
        where,
        order,
        limit,
        offset
    }).then(result => {
        res.send({
            success: true,
            result: result
        });
    }).catch(err => {
        next(err);
    });
}

function getRoleId(roleName) {
    let roleId = null;
    switch (roleName) {
        case 'administrator':
            roleId = 1;
            break;
        case 'nurse':
            roleId = 2;
            break;
    }
    return roleId;
}