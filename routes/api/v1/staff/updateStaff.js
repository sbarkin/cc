'use strict';

const saveUser = require('../user/saveUser');

/**
 *  @swagger
 *  /v1/staff/{staffId}:
 *      put:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - staff
 *          summary: update Staff
 *          description: update staff
 *          parameters:
 *              -   in: path
 *                  name: staffId
 *                  required: true
 *                  description: staff id
 *              -   name: body
 *                  in: body
 *                  description: Staff data
 *                  schema:
 *                      type: object
 *                      properties:
 *                          first_name:
 *                              type: string
 *                          last_name: 
 *                              type: string
 *                          email:
 *                              type: string
 *                          password:
 *                              type: string
 *                          user_roles_id:
 *                              type: number 
 *                          user_statuses_id:
 *                              type: number
 *          responses:
 *              204:
 *                  description: Successful operation
 *              400:
 *                  description: Staff id is required.
 *              403:
 *                  description: You not have the permissions for this action
 *              404:
 *                  description: Staff not found 
 */
module.exports = (req, res, next) => {
    if (!req.params || !req.params.staffId) {
        return res.status(400).json({
            success: false,
            message: 'Staff id is required.'
        });
    }
    let staffId = req.params.staffId;

    if (req.userType !== 'user' || req.user_roles_id !== 1) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    saveUser(staffId, req.body, res).catch(err => {
        next(err);
    });
}