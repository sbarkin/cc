'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;

/**
 *  @swagger
 *  /v1/staff/{staffId}/total-actions:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - staff
 *          summary: Get total actions for staff
 *          description: Return total actions for staff
 *          parameters:
 *              -   in: path
 *                  name: staffId
 *                  required: true
 *                  description: staff id
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success:
 *                              type: boolean
 *                          total:
 *                              type: number
 *              400:
 *                  description: Staff id is required 
 *              403:
 *                  description: You not have the permissions for this action 
 */
module.exports = (req, res, next) => {
    if (!req.params || !req.params.staffId) {
        return res.status(400).json({
            success: false,
            message: 'Staff id is required.'
        });
    }
    if (req.userType !== 'user') {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    models.calls.findAll({
        attributes: [
            [models.Sequelize.fn('count', models.Sequelize.col('id')), 'total']
        ],
        where: {
            action_by_users_id: req.params.staffId
        },
    }).then(result => {
        res.send({
            success: true,
            result: result
        });
    }).catch(err => {
        next(err);
    });
}