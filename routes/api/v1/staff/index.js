'use strict';

const express = require('express');
const router = express.Router();

const getStaff = require('./getStaff');
const newStaff = require('./newStaff');
const updateStaff = require('./updateStaff');
const getTotalActions = require('./getTotalActions');

module.exports = router;

router.get('/', getStaff);
router.post('/', newStaff);
router.put('/:staffId', updateStaff);
router.get('/:staffId/total-actions', getTotalActions);