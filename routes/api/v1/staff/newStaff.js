'use strict';

let models = require('./../../../../models');
const Op = models.Sequelize.Op;
var commonService = require('./../../../../lib/common.service');

/**
 *  @swagger
 *  /v1/staff:
 *      post:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - staff
 *          summary: Add new staff
 *          description: Return staff id
 *          parameters:
 *              -   name: body
 *                  required: true
 *                  description: Staff object
 *                  in: body
 *                  schema:
 *                      type: object
 *                      required:
 *                          - first_name
 *                          - last_name
 *                          - email
 *                          - password
 *                          - user_roles_id
 *                      properties:
 *                          first_name:
 *                              type: string
 *                          last_name: 
 *                              type: string
 *                          email:
 *                              type: string
 *                          password:
 *                              type: string
 *                          user_roles_id:
 *                              type: number
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                      type: object
 *                      properties:
 *                          success: 
 *                              type: boolean   
 *                          id:
 *                              type: number
 *              400:
 *                  description: first_name, last_name, email, password, user_roles_id are required 
 *              403:
 *                  description: You not have the permissions for this action
 */
module.exports = (req, res, next) => {
    if (!req.body || !req.body.first_name || !req.body.last_name || !req.body.email || !req.body.password || !req.body.user_roles_id) {
        return res.status(400).json({
            success: false,
            message: 'first_name, last_name, email, password, user_roles_id are required.'
        });
    }
    if (req.userType !== 'user' || req.user_roles_id !== 1) {
        return res.status(403).json({
            success: false,
            message: 'You not have the permissions for this action.'
        })
    }
    const user_statuses_id = 1;
    commonService.setHash(req.body.password)
        .then(hash => {
            return models.users.create({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: hash,
                user_roles_id: req.body.user_roles_id,
                user_statuses_id
            });
        })
        .then(result => {
            global.socketServer.sendMessageToUsers('updateUsers');
            res.send({
                success: true,
                id: result.id
            });
        }).catch(err => {
            next(err);
        });
}