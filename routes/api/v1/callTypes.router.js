'use strict';

const express = require('express');
const router = express.Router();
let models = require('./../../../models');
const Op = models.Sequelize.Op;

module.exports = router;

/**
 *  @swagger
 *  /v1/call-types:
 *      get:
 *          security:
 *              - Bearer: [] 
 *          tags:
 *              - call types
 *          summary: List call types
 *          description: Returns list call types.
 *          responses:
 *              200:
 *                  description: Successful operation
 *                  schema:
 *                       $ref: '#/definitions/CallTypes'
 *              403:
 *                  description: No token provided.
 */
router.get('/', (req, res, next) => {
    models.call_types.findAll({
        order: [
            ['id', 'ASC']
        ]
    }).then(result => {
        res.send(result);
    }).catch(err => {
        next(err);
    });

});