'use strict';

const express = require('express');
const router = express.Router();
const config = require('./../../config/config');

module.exports = router;

const options = {
    swaggerDefinition: {
        info: {
            title: 'CommuniCare REST - Swagger',
            version: '1.0.0',
            description: 'CommuniCare REST API with Swagger doc',
            contact: {
                email: 'svolodko@gmail.com'
            }
        },
        schemes: ['http'],
        securityDefinitions: {
            Bearer: {
                type: 'apiKey',
                name: 'x-access-token',
                in: 'header'
            }
        },
        host: config.swagger.baseName + ':' + config.apiPort,
        basePath: '/api',
        tags: [{
            name: 'auth',
            description: 'User authentication'
        }, {
            name: 'call types',
            description: 'Call types'
        }, {
            name: 'user roles',
            description: 'User roles'
        }, {
            name: 'patient',
            description: 'Patient requests for mobile App'
        }, {
            name: 'user',
            description: 'User requests'
        }]
    },
    apis: ['./routes/api/auth/auth.router.js',
        './routes/api/auth/auth.forgot-password.js',
        './routes/api/auth/auth.check-reset-password-token.js',
        './routes/api/auth/auth.reset-password.js',
        './routes/api/auth/auth.request-access.js',
        './routes/api/v1/callTypes.router.js',
        './models/call_types.js',
        './routes/api/v1/patient/createCall.js',
        './models/calls.js',
        './routes/api/v1/patient/getPatient.js',
        './models/patients.js',
        './routes/api/v1/patient/getLastCall.js',
        './routes/api/v1/patient/cancelCall.js',
        './routes/api/v1/calls/getTotalCalls.js',
        './routes/api/v1/calls/getCalls.js',
        './routes/api/v1/calls/setAction.js',
        './routes/api/v1/user/getUser.js',
        './routes/api/v1/user/updateUser.js',
        './routes/api/v1/user/subscribeNotification.js',
        './routes/api/v1/user/unsubscribeNotification.js',
        './models/users.js',
        './routes/api/v1/patients/getPatients.js',
        './routes/api/v1/staff/getStaff.js',
        './routes/api/v1/user-roles/getUserRoles.js',
        './models/user_roles.js',
        './routes/api/v1/patients/newPatient.js',
        './routes/api/v1/patients/updatePatient.js',
        './routes/api/v1/patients/getTotalCalls.js',
        './routes/api/v1/staff/newStaff.js',
        './routes/api/v1/staff/updateStaff.js',
        './routes/api/v1/staff/getTotalActions.js'
    ]
}

const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const swaggerSpec = swaggerJSDoc(options)

router.get('/json', function(req, res) {
    res.setHeader('Content-Type', 'application/json')
    res.send(swaggerSpec)
})

router.use('/', swaggerUi.serve, swaggerUi.setup(swaggerSpec));