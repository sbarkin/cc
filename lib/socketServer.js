let config = require('./../config/config');
let express = require('express');
let cors = require('cors');
let http = require('http');
let socketIo = require('socket.io');

class SocketServer {

    constructor() {
        this.activePatients = {};
        this.activeUsers = {};
        this.server = this.createServer();
        this.io = socketIo(this.server);
        this.patientNameSpace = this.io.of('/patient');
        this.initPatientEvents();
        this.userNameSpace = this.io.of('/user');
        this.initUserEvents();
    }

    start() {
        this.server.listen(config.socketIoPort || 3001, () => {
            console.log('Socketio server is listening on', config.socketIoPort || 3001);
        });
    }

    createServer() {
        let socketIoApp = express();
        socketIoApp.use(cors());
        return http.createServer(socketIoApp);
    }

    initPatientEvents() {
        let self = this;
        this.patientNameSpace.on('connection', function(socket) {
            self.createPatient(socket.id);
            socket.on('setPatient', function(user, fn) {
                self.setPatient(socket.id, user);

                // this done for unit test
                if (typeof(fn) === 'function') {
                    fn(user);
                }
            });
            socket.on('disconnect', function() {
                self.removePatient(socket.id);
            });
        });
    }

    sendMessagePatientid(id, msgName, message) {
        const self = this;
        for (let index in this.activePatients) {
            let item = self.activePatients[index];
            if (item.patient && item.patient.id === Number(id)) {
                self.patientNameSpace.sockets[index].emit(msgName, message);
            }
        }
    }

    createPatient(id) {
        this.activePatients[id] = {};
    }

    setPatient(id, userData) {
        if (this.activePatients[id]) {
            this.activePatients[id].patient = userData;
        }
    }

    removePatient(id) {
        if (this.activePatients[id]) {
            delete this.activePatients[id];
        }
    }

    initUserEvents() {
        let self = this;
        this.userNameSpace.on('connection', function(socket) {
            self.createUser(socket.id);
            socket.on('setUser', function(user, fn) {
                self.setUser(socket.id, user);

                // this done for unit test
                if (typeof(fn) === 'function') {
                    fn(user);
                }
            });
            socket.on('disconnect', function() {
                self.removeUser(socket.id);
            });
        });
    }

    sendMessageToUsers(msgName, message) {
        this.userNameSpace.emit(msgName, message);
    }

    sendMessageUserid(id, msgName, message) {
        const self = this;
        for (let index in this.activeUsers) {
            let item = self.activeUsers[index];
            if (item.user && item.user.userId === Number(id)) {
                self.userNameSpace.sockets[index].emit(msgName, message);
            }
        }
    }

    createUser(id) {
        this.activeUsers[id] = {};
    }

    setUser(id, userData) {
        if (this.activeUsers[id]) {
            this.activeUsers[id].user = userData;
        }
    }

    removeUser(id) {
        if (this.activeUsers[id]) {
            delete this.activeUsers[id];
        }
    }

    closeServer() {
        this.server.close();
    }
}

module.exports = new SocketServer();