const bcrypt = require('bcrypt');
const saltRounds = 10;

class CommonService {

    setHash(data) {
        return bcrypt.hash(data, saltRounds);
    }

    compareHash(data, hash) {
        return bcrypt.compare(data, hash);
    }
}
module.exports = new CommonService();