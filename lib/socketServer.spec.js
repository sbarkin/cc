'user strict';

let socketIo = require('./socketServer');
let io = require('socket.io-client');
let ioOptions = {
    transports: ['websocket'],
    forceNew: true,
    reconnection: false
};
let config = require('./../config/config');
let socketIoUrl = 'http://localhost:' + config.socketIoPort;
let patientClient;
let expectedPatient;

/**
 * @test {SocketServer}
 */
describe('Socket server', done => {
    before(done => {
        socketIo.start();
        expectedPatient = {
            id: 1,
            units_id: 1,
            first_name: 'Peter',
            last_name: 'Berkly',
            last_four_sn: 1234,
            room_number: '1',
            patient_actions_id: 1
        }
        done();
    });

    beforeEach(done => {
        patientClient = io(socketIoUrl + '/patient', ioOptions);
        patientClient.on('connect', () => {
            done();
        });
        patientClient.on('connect_error', (error) => {
            done();
        });
    });

    afterEach(done => {
        patientClient.disconnect();
        done();
    });

    after(() => {
        socketIo.closeServer();
    });

    it('should client be connected and patient socketid be added to activePatients', () => {
        expect(patientClient.connected).to.equal(true);
        expect(socketIo.activePatients).to.have.property(patientClient.id);
    });

    it('should add patient data by client emit setPatient', done => {
        patientClient.emit('setPatient', expectedPatient, data => {
            expect(socketIo.activePatients[patientClient.id]).to.have.property('patient');
            expect(socketIo.activePatients[patientClient.id].patient).to.eql(expectedPatient);
            done();
        });
    });

    it('should remove user data from activePatient on disconnect patient', done => {
        let patientId = patientClient.id;
        patientClient.disconnect();
        setTimeout(() => {
            expect(socketIo.activePatients).to.not.have.property(patientId);
            done();
        }, 100);
    });

    it('should emit message for specific socketId', done => {
        let message = 'Test message for ' + patientClient.id;
        patientClient.on('testMessage', data => {
            expect(data).to.equal(message);
            done();
        });
        socketIo.sendMessagePatientid(patientClient.id, 'testMessage', message);
    });

});