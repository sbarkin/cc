let config = require('./../config/config');
const webpush = require('web-push');
let models = require('./../models');
const Op = models.Sequelize.Op;

class WebPushService {
    constructor() {
        this._webpush = webpush;
        this._webpush.setVapidDetails(
            'mailto:' + config.webPush.mailTo,
            config.webPush.vapidKeys.publicKey,
            config.webPush.vapidKeys.privateKey
        );
    }

    sendNotificationAllUsers(title, body, url) {
        const notificationPayload = {
            "notification": {
                "title": title,
                "body": body,
                "icon": "assets/imgs/logo_cc.png",
                "vibrate": [300, 100, 400],
                "data": {
                    "url": url
                },
                requireInteraction: true
            }
        };
        models.user_subscriptions.findAll({
            where: {
                expired: {
                    [Op.gt]: Date.now()
                }
            }
        }).then(subs => {

            return Promise.all(subs.map(sub => webpush.sendNotification({
                endpoint: sub.endpoint,
                keys: {
                    auth: sub.auth,
                    p256dh: sub.p256dh
                }
            }, JSON.stringify(notificationPayload))));

        }).catch(err => {
            console.log('error send notification:', err);
        });
    }
}

module.exports = new WebPushService();