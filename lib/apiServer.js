'use strict';

const config = require('./../config/config');
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const http = require('http');
const routes = require('./../routes');

class ApiServer {
    constructor() {
        this._app = express();
        this._app.use(bodyParser.json());
        this._app.use(cors());
        routes(this._app);
        this.server = http.createServer(this._app);
    }

    start() {
        this.server.listen(config.apiPort || 3000, () => {
            console.log('Api server is listening on', config.rpcPort || 3000);
        });
    }

    get app() {
        return this._app;
    }

}
module.exports = new ApiServer();