import { TestBed, inject } from '@angular/core/testing';

import { CallTypesService } from './call-types.service';

describe('CallTypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CallTypesService]
    });
  });

  it('should be created', inject([CallTypesService], (service: CallTypesService) => {
    expect(service).toBeTruthy();
  }));
});
