import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiUrl } from '../config';

@Injectable()
export class CallTypesService {

  constructor(private httpClient: HttpClient) { }

  getCallTypes(): Observable<any> {
    return this.httpClient.get(`${apiUrl}/v1/call-types`);
  }
}
