import { Router } from '@angular/router';
import { Staff } from './../../models/staff';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { apiUrl } from '../config';
import { UserData } from '../../models/user-data';

@Injectable()
export class UserService {
  private userData = new ReplaySubject<Object>(1);
  private ccUserData: UserData;
  constructor(private http: HttpClient,
    private router: Router
  ) {
    const ccUserData = JSON.parse(localStorage.getItem('ccUser'));
    if (ccUserData) {
      this.ccUserData = ccUserData;
      this.getCurrentUser();
    } else {
      this.userData.next(false);
    }

  }

  getCurrentUser() {
    this.getUserInfo().subscribe(userInfo => {
      this.ccUserData.user = userInfo;
      this.userData.next(this.ccUserData);
    }, error => {
      console.log('error getUserInfo:', error);
      this.userData.next(false);
    });
  }

  getUserData(): Observable<Object> {
    return this.userData.asObservable();
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(`${apiUrl}/auth/user`, {username, password})
      .pipe(
        mergeMap((result: any) => {
          if (result.success && result.token && result.userId) {
            this.ccUserData = <UserData> {
              userId: result.userId,
              token: result.token
            };
            localStorage.setItem('ccUser', JSON.stringify(this.ccUserData));
            return this.getUserInfo();
          } else {
            return Observable.throw({success: false});
          }
        }),
        map((userInfo: any) => {
          this.ccUserData.user = userInfo;
          this.userData.next(this.ccUserData);
          return {success: true};
        }),
        catchError(err => {
          let error = {success: false};
          if (err.status === 401 && err.error) {
            error = err.error;
          }
          return Observable.of(error);
        })
      );
  }

  logout() {
    if (this.router.url !== '/login') {
      localStorage.clear();
      this.userData.next(false);
      this.router.navigate(['login']);
    }
  }

  editUser(user: Staff): Observable<boolean> {
    const body = {...user};
    delete body.id;
    return this.http.put(`${apiUrl}/v1/user/current`, body)
    .pipe(
      map((result: any) => {
        return true;
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }

  forgotPassword(username) {
    return this.http.post(`${apiUrl}/auth/forgot-password`, {username})
      .pipe(
        map((result: any) => {
          return result;
        }),
        catchError(err => {
          console.log(err);
          return Observable.of(false);
        })
      );
  }

  checkResetPasswordToken(token) {
    return this.http.get(`${apiUrl}/auth/reset-password/${token}/check`)
      .pipe(
        map((result: any) => {
          return result;
        }),
        catchError(err => {
          console.log(err);
          return Observable.of(false);
        })
      );
  }

  updatePassword(user: Staff): Observable<boolean> {
    const body = {
      password: user.password
    };
    return this.http.put(`${apiUrl}/auth/reset-password/${user.reset_password_token}`, body)
    .pipe(
      map((result: any) => {
        return true;
      }),
      catchError(err => {
        console.log('error reset password:', err);
        return Observable.of(false);
      })
    );
  }

  requestAccess(name, email) {
    return this.http.post(`${apiUrl}/auth/request-access`, {name, email})
      .pipe(
        map((result: any) => {
          return result;
        }),
        catchError(err => {
          console.log(err);
          return Observable.of(false);
        })
      );
  }

  private getUserInfo(): Observable<any> {
    return this.http.get(`${apiUrl}/v1/user/current`);
  }
}
