import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { SwPush, SwUpdate } from '@angular/service-worker';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { apiUrl, webPushPublicKey } from '../config';
import { map, switchMap, take, catchError } from 'rxjs/operators';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { of } from 'rxjs/observable/of';

@Injectable()
export class ServiceWorkerService {

  sub: PushSubscription;

  constructor(private swPush: SwPush,
    private swUpdate: SwUpdate,
    private http: HttpClient
  ) {}

  initSW() {
    if (environment.production && 'serviceWorker' in navigator) {
      navigator.serviceWorker.getRegistration()
        .then(active => {
          if (!active) {
            navigator.serviceWorker.register('/ngsw-worker.js');
          }
          this.checkUpdate();
        }).catch(err => {
          console.log('error SW registration:', err);
        });
    }
  }

  subscribeToPush() {
    this.swPush.requestSubscription({
      serverPublicKey: webPushPublicKey
    })
      .then(sub => {
        this.sub = sub;
        // Passing subscription object to our backend
        this.addPushSubscriber(sub).subscribe(
          () => console.log('Sent push subscription object to server.'),
          err => console.log('Could not send subscription object to server, reason: ', err)
        );

      }).catch(err => {
        console.error('err swPush:', err);
      });

  }

  addPushSubscriber(sub: any) {
    return this.http.post(`${apiUrl}/v1/user/current/notification`, sub);
  }

  deletePushSubscriber(sub: any) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: sub
    };
    return this.http.delete(`${apiUrl}/v1/user/current/notification`, httpOptions);
  }

  checkUpdate() {
      if (this.swUpdate.isEnabled) {
        this.swUpdate.available.subscribe(() => {
            if (confirm('New version available. Load New Version?')) {
                window.location.reload();
            }
        }, err => {
          console.log('error subscribe swUpdate', err);
        });
    }
  }

  unsubscribeFromPush() {
    let pushSubscription: PushSubscription;
    if (!this.swPush.isEnabled || !this.sub) {
      return of(false);
    } else {
    return this.swPush.subscription
      .pipe(
        take(1),
        switchMap((sub: PushSubscription) => {
          pushSubscription = sub;
          return this.deletePushSubscriber(sub);
        }),
        map((res) => {
          return fromPromise(pushSubscription.unsubscribe())
            .pipe(
              catchError(error => of(error))
            );
        }),
        catchError(error => {
          console.log('error unsubscribe: ', error);
          return of(error);
        })
      );
    }
  }

}
