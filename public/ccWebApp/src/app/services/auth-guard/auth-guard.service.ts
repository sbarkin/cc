import { UserData } from './../../models/user-data';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { UserService } from './../user/user.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';


@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor(private userService: UserService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const url: string = state.url;
    const path = route.url.length ? route.url[0].path : '';
    if (['login', 'forgot-password', 'request-access', 'reset-password'].indexOf(path) !== -1) {
      return this.checkActivateAuthUrl(route.url);
    } else {
      return this.checkLogin(url);
    }
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const url: string = state.url;
    return this.checkLogin(url);
  }

  checkLogin(url: string): Observable<boolean> {
    return this.userService.getUserData().pipe(
      map((result: UserData) => {
          if (!result) {
            this.router.navigate(['/login']);
            return false;
          } else {
            if (this.isAdminUrl(url) && result.user.user_roles_id !== 1) {
              this.router.navigate(['/']);
              return false;
            } else {
              return true;
            }
          }
      })
    );
  }

  private isAdminUrl(url) {
    return /^(\/users|\/admin)/.test(url);
  }

  checkActivateAuthUrl(url: Array<any>): Observable<boolean> {
    return this.userService.getUserData().pipe(
      map((result: UserData) => {
          if (result) {
            if (url[0].path === 'reset-password' && result.user.user_roles_id === 1) {
              return true;
            } else {
              this.router.navigate(['/']);
              return false;
            }
          } else {
            return true;
          }
      })
    );
  }
}
