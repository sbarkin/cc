import { Observable } from 'rxjs/Observable';
import { UserService } from './../user/user.service';
import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class SocketService {
  private userSubscription: Subscription;
  constructor(private socket: Socket,
    private userService: UserService
  ) {}

  startSocketClient() {
    this.socket.on('connect', () => {
      this.userSubscription = this.userService.getUserData().subscribe(data => {
        this.socket.emit('setUser', data);
      });
    });
    this.socket.on('disconnect', () => {
      this.userSubscription.unsubscribe();
    });

    this.socket.on('updateUser', data => {
      this.userService.getCurrentUser();
    });
  }

  sendMessage(msgName: string, msg: string) {
    this.socket.emit(msgName, msg);
  }

  getMessage(msgName: string): Observable<Object> {
    return this.socket
        .fromEvent(msgName);
  }

}
