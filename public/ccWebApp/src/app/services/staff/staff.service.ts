import { Staff } from './../../models/staff';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { map, catchError } from 'rxjs/operators';
import { apiUrl } from '../config';
import { StaffRequestParams } from '../../models/staff-request-params';

@Injectable()
export class StaffService {

  constructor(private httpClient: HttpClient) { }

  loadStaff(state: string = 'active', reqParams: StaffRequestParams): Observable<[Staff]> {
    let params = new HttpParams().set('state', state);
    const emptyResult = {
      count: 0,
      rows: []
    };
    Object.keys(reqParams).forEach(key => params = params.set(key, reqParams[key]));
    return this.httpClient.get(`${apiUrl}/v1/staff`, {params})
    .pipe(
      map((result: any) => {
        if (result.success) {
          return result.result;
        } else {
          return emptyResult;
        }
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(emptyResult);
      })
    );
  }

  newStaff(staff: Staff): Observable<boolean> {
    return this.httpClient.post(`${apiUrl}/v1/staff`, staff)
    .pipe(
      map((result: any) => {
        return true;
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }

  editStaff(staff: Staff): Observable<boolean> {
    const staffId = staff.id;
    const body = {...staff};
    delete body.id;
    return this.httpClient.put(`${apiUrl}/v1/staff/${staffId}`, body)
    .pipe(
      map((result: any) => {
        return true;
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }

  getTotalActions(staffId: number) {
    return this.httpClient.get(`${apiUrl}/v1/staff/${staffId}/total-actions`)
    .pipe(
      map((result: any) => {
        if (result.success) {
          return result.result[0];
        } else {
          return false;
        }
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }
}
