import { SocketService } from './../socket/socket.service';
import { Subject } from 'rxjs/Subject';
import { CallRequestParams } from './../../models/call-request-params';
import { Observable } from 'rxjs/Observable';
import { CallsTotal } from './../../models/calls-total';
import { Call } from './../../models/call';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { map, catchError } from 'rxjs/operators';
import { apiUrl } from '../config';

import {
  debounceTime
} from 'rxjs/operators';

@Injectable()
export class CallsService {

  private openCallsTotal = new BehaviorSubject<CallsTotal>(<CallsTotal>{});
  private searchInput = new Subject<string>();
  constructor( private httpClient: HttpClient,
    private socketService: SocketService
  ) {
    this.loadData();
    this.socketService.getMessage('updateCall').pipe(
      debounceTime(300),
    ).subscribe(data => {
      this.loadData();
    });
  }

  private loadData() {
    this.loadOpenCallsTotal().subscribe();
  }

  setSearchInput(searchText: string) {
    this.searchInput.next(searchText);
  }

  getSearchInput() {
    return this.searchInput.asObservable();
  }

  getOpenCallsTotal(): Observable<CallsTotal> {
    return this.openCallsTotal.asObservable();
  }

  loadCalls(state: string = 'open', reqParams: CallRequestParams): Observable<[Call]> {
    let params = new HttpParams().set('state', state);
    const emptyResult = {
      count: 0,
      rows: []
    };
    Object.keys(reqParams).forEach(key => params = params.set(key, reqParams[key]));
    return this.httpClient.get(`${apiUrl}/v1/calls`, {params})
    .pipe(
      map((result: any) => {
        if (result.success) {
          return result.result;
        } else {
          return emptyResult;
        }
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(emptyResult);
      })
    );
  }

  resolveCall(callId: number, actionId: number, note: string = null): Observable<boolean> {
    const body = <any>{};
    if (note) {
      body.note = note;
    }
    return this.httpClient.put(`${apiUrl}/v1/calls/${callId}/action/${actionId}`, body)
    .pipe(
      map((result: any) => {
        return true;
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }

  loadOpenCallsTotal(): Observable<boolean> {
    const params = new HttpParams().set('state', 'open');
    return this.httpClient.get(`${apiUrl}/v1/calls/total`, {params})
    .pipe(
      map((result: any) => {
        if (result.success) {
          this.processCallTotal(result.result);
          return true;
        } else {
          return false;
        }
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }

  processCallTotal(data): void {
    const callsTotal = <CallsTotal>{};
    let total = 0;
    data.forEach(item => {
      switch (item.call_types_id) {
        case 1:
          callsTotal.pain = item.total;
          break;
        case 2:
          callsTotal.bathroom = item.total;
          break;
        case 3:
          callsTotal.drink = item.total;
          break;
        case 4:
          callsTotal.tvRemote = item.total;
          break;
        case 5:
          callsTotal.question = item.total;
          break;
      }
      total += item.total;
    });
    callsTotal.all = total;
    this.openCallsTotal.next(callsTotal);
  }
}
