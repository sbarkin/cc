import { DialogParams } from './../../models/dialog-params';
import { DialogModalComponent } from './../../components/dialog-modal/dialog-modal.component';
import { Injectable } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Injectable()
export class DialogService {

  constructor(private modalService: BsModalService) { }

  openDialog(params: DialogParams) {
    const modalRef: BsModalRef = this.modalService.show(DialogModalComponent, {
      initialState: params,
      class: 'modal-dialog-centered'
    });
    return modalRef;
  }
}
