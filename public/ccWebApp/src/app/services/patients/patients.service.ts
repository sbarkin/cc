import { Patient } from './../../models/patient';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/observable/of';
import { map, catchError } from 'rxjs/operators';
import { apiUrl } from '../config';
import { PatientsRequestParams } from '../../models/patients-request-params';

@Injectable()
export class PatientsService {

  constructor(private httpClient: HttpClient) { }

  loadPatients(state: string = 'active', reqParams: PatientsRequestParams): Observable<[Patient]> {
    let params = new HttpParams().set('state', state);
    const emptyResult = {
      count: 0,
      rows: []
    };
    Object.keys(reqParams).forEach(key => params = params.set(key, reqParams[key]));
    return this.httpClient.get(`${apiUrl}/v1/patients`, {params})
    .pipe(
      map((result: any) => {
        if (result.success) {
          return result.result;
        } else {
          return emptyResult;
        }
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(emptyResult);
      })
    );
  }

  newPatient(patient: Patient): Observable<boolean> {
    return this.httpClient.post(`${apiUrl}/v1/patients`, patient)
    .pipe(
      map((result: any) => {
        return true;
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }

  editPatient(patient: Patient): Observable<boolean> {
    const patientId = patient.id;
    const body = {...patient};
    delete body.id;
    return this.httpClient.put(`${apiUrl}/v1/patients/${patientId}`, body)
    .pipe(
      map((result: any) => {
        return true;
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }

  getPatientTotalCalls(patientId: number) {
    return this.httpClient.get(`${apiUrl}/v1/patients/${patientId}/total-calls`)
    .pipe(
      map((result: any) => {
        if (result.success) {
          return result.result[0];
        } else {
          return false;
        }
      }),
      catchError(err => {
        console.log(err);
        return Observable.of(false);
      })
    );
  }

}
