import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiUrl } from '../config';

@Injectable()
export class UserRolesService {

  constructor(private httpClient: HttpClient) { }

  getUserRoles(): Observable<any> {
    return this.httpClient.get(`${apiUrl}/v1/user-roles`);
  }
}
