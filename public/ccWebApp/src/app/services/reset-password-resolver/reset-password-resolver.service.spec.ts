import { TestBed, inject } from '@angular/core/testing';

import { ResetPasswordResolverService } from './reset-password-resolver.service';

describe('ResetPasswordResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResetPasswordResolverService]
    });
  });

  it('should be created', inject([ResetPasswordResolverService], (service: ResetPasswordResolverService) => {
    expect(service).toBeTruthy();
  }));
});
