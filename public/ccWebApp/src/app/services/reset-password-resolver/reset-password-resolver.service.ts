import { Staff } from './../../models/staff';
import { UserService } from './../user/user.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { Router, Resolve, RouterStateSnapshot,
  ActivatedRouteSnapshot } from '@angular/router';
import { StaffRequestParams } from '../../models/staff-request-params';

@Injectable()
export class ResetPasswordResolverService implements Resolve<Staff> {

  constructor(private router: Router,
    private userService: UserService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Staff> {
    const token = route.paramMap.get('token');

    return this.userService.checkResetPasswordToken(token).pipe(
      map(data => {
        if (data) {
          return data;
        } else { // token not found
          // this.router.navigate(['/']);
          return {
            id: null,
            user_roles_id: null
          };
        }
      })
    );
  }
}
