import { ServiceWorkerService } from './services/service-worker/service-worker.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';

import { AuthHttpInterceptor } from './auth-http-interceptor';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { environment } from '../environments/environment';

import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { DialogModalComponent } from './components/dialog-modal/dialog-modal.component';
import { UserService } from './services/user/user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { BsDropdownModule } from 'ngx-bootstrap';

import fontawesome from '@fortawesome/fontawesome';
import light from '@fortawesome/fontawesome-pro-light';
import solid from '@fortawesome/fontawesome-pro-solid';
import { CallsService } from './services/calls/calls.service';
import { SocketService } from './services/socket/socket.service';
fontawesome.library.add(light, solid);

import { socketIoUrl } from './services/config';
import { CallTypesService } from './services/call-types/call-types.service';
import { PatientsService } from './services/patients/patients.service';
import { StaffService } from './services/staff/staff.service';
import { UserRolesService } from './services/user-roles/user-roles.service';
import { ProfileComponent } from './components/profile/profile.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ResetPasswordResolverService } from './services/reset-password-resolver/reset-password-resolver.service';
import { RequestAccessComponent } from './components/request-access/request-access.component';
const socketIoConfig: SocketIoConfig = { url: socketIoUrl, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    DialogModalComponent,
    ProfileComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    RequestAccessComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    BsDropdownModule.forRoot(),
    SharedModule,
    SocketIoModule.forRoot(socketIoConfig)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true
    },
    UserService,
    CallsService,
    SocketService,
    CallTypesService,
    PatientsService,
    StaffService,
    UserRolesService,
    ResetPasswordResolverService,
    ServiceWorkerService
  ],
  entryComponents: [
    DialogModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
