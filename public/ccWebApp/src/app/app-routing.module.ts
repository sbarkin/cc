import { RequestAccessComponent } from './components/request-access/request-access.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ResetPasswordResolverService } from './services/reset-password-resolver/reset-password-resolver.service';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'request-access',
    component: RequestAccessComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'reset-password/:token',
    component: ResetPasswordComponent,
    canActivate: [AuthGuardService],
    resolve: {
      user: ResetPasswordResolverService
    }
  },
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: 'calls',
        canActivateChild: [AuthGuardService],
        loadChildren: 'app/calls/calls.module#CallsModule'
      },
      {
        path: 'users',
        canActivateChild: [AuthGuardService],
        loadChildren: 'app/users/users.module#UsersModule'
      },
      {
        path: 'admin',
        canActivateChild: [AuthGuardService],
        loadChildren: 'app/admin/admin.module#AdminModule'
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: '',
        redirectTo: 'calls',
        pathMatch: 'prefix'
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,
      { enableTracing: false } // <-- debugging purposes only
  )],
  exports: [ RouterModule ],
  providers: [AuthGuardService]
})
export class AppRoutingModule { }
