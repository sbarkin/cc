import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolvedCallsComponent } from './resolved-calls.component';

describe('ResolvedCallsComponent', () => {
  let component: ResolvedCallsComponent;
  let fixture: ComponentFixture<ResolvedCallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolvedCallsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolvedCallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
