import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { CallsService } from './../../services/calls/calls.service';
import { Subject } from 'rxjs/Subject';
import { CallTypes } from './../../models/call-types';
import { Call } from './../../models/call';
import { CallRequestParams } from './../../models/call-request-params';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

@Component({
  selector: 'app-resolved-calls',
  templateUrl: './resolved-calls.component.html',
  styleUrls: ['./resolved-calls.component.scss']
})
export class ResolvedCallsComponent implements OnInit, OnDestroy {
  filterCallTypes: Array<CallTypes> = [];
  filterTimeRange = '';
  searchInput = new Subject<string>();
  searchInputSubscription: Subscription;
  searchStaff = new Subject<string>();
  searchStaffSubscription: Subscription;
  resolvedCallsNumber = 0;
  calls: Array<Call> = [];
  callType = 'all';
  timeRange = 'all';
  sortOrder = 'desc';
  sortBy = 'timereceived';
  offset = 0;
  searchText = '';
  searchStaffText = '';
  constructor(private callsService: CallsService) { }

  ngOnInit() {
    this.searchInputSubscription = this.searchInput.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term => {
        this.searchText = term;
        this.offset = 0;
        return this.getCalls();
      })
    ).subscribe();

    this.searchStaffSubscription = this.searchStaff.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term => {
        this.searchStaffText = term;
        this.offset = 0;
        return this.getCalls();
      })
    ).subscribe();

    this.getCalls().subscribe();
  }


  ngOnDestroy() {
    this.searchInputSubscription.unsubscribe();
    this.searchStaffSubscription.unsubscribe();
  }

  setFilter(filterData) {
    if (filterData.callTypes) {
     this.callType = filterData.callTypes.map(item => item.short_name.toLowerCase().replace('/', '')).join();
    } else {
      this.callType = 'all';
    }

    this.timeRange = filterData.timeRange ? filterData.timeRange : 'all';

    this.offset = 0;
    this.getCalls().subscribe();
  }

  setSortBy(sortBy: string) {
    if (this.sortBy === sortBy) {
      this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortBy = sortBy;
      this.sortOrder = 'asc';
    }
    this.offset = 0;
    this.getCalls().subscribe();
  }

  onScroll() {
    this.offset += 20;
    this.getCalls().subscribe();
  }

  private getCalls(): Observable<boolean> {
    const params = <CallRequestParams>{};
    if (this.callType !== 'all') {
      params.type = this.callType;
    }
    if (this.timeRange !== 'all') {
      params.range = this.timeRange;
    }
    params['sort-by'] = this.sortBy;
    params['sort-order'] = this.sortOrder;
    params['offset'] = this.offset;
    if (this.searchText) {
      params['q'] = this.searchText;
    }
    if (this.searchStaffText) {
      params['q-staff'] = this.searchStaffText;
    }
    return this.callsService.loadCalls('resolved', params)
      .pipe(
        map((result: any) => {
          if (this.offset) {
            this.calls = this.calls.concat(result.rows);
          } else {
            this.calls = result.rows;
          }
          this.resolvedCallsNumber = result.count;
          return true;
        })
      );
  }

}
