import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterResolvedCallsComponent } from './filter-resolved-calls.component';

describe('FilterResolvedCallsComponent', () => {
  let component: FilterResolvedCallsComponent;
  let fixture: ComponentFixture<FilterResolvedCallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterResolvedCallsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterResolvedCallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
