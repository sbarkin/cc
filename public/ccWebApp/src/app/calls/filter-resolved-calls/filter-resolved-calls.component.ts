import { CallTypesService } from './../../services/call-types/call-types.service';
import { CallTypes } from './../../models/call-types';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Subject } from 'rxjs/Subject';

const timeRanges = [
  'All',
  '1 Week',
  '4 Weeks',
  '1 Year',
  'Mtd',
  'Ytd'
];

@Component({
  selector: 'app-filter-resolved-calls',
  templateUrl: './filter-resolved-calls.component.html',
  styleUrls: ['./filter-resolved-calls.component.scss']
})
export class FilterResolvedCallsComponent implements OnInit {
  @ViewChild('searchStaff') searchStaffInput: ElementRef;
  callTypes: Array<CallTypes> = [];
  @Input() resolvedCallsNumber = 0;
  @Input() searchInputSubject: Subject<string>;
  @Input() searchStaffSubject: Subject<string>;
  @Output() filterChange = new EventEmitter();
  callTypeButtonTitle = 'All';
  timeRanges = timeRanges;
  currentRange = 'All';
  staff = '';
  constructor(private callTypesService: CallTypesService) { }

  ngOnInit() {
    this.callTypesService.getCallTypes().subscribe(data => {
      this.callTypes = data;
    });
  }

  callTypeChange() {
    const checkedTypes = this.callTypes.filter(item => item.checked);
    if (checkedTypes.length === 0) {
      this.callTypeButtonTitle = 'All';
    } else if (checkedTypes.length === 1) {
      this.callTypeButtonTitle = checkedTypes[0].short_name;
    } else {
      this.callTypeButtonTitle = 'Multiple';
    }
    this.filterChange.emit(this.getFilter());
  }

  timeRangesChange() {
    this.filterChange.emit(this.getFilter());
  }

  private getFilter(): {} {
    const filter = {
      callTypes: this.callTypes.filter(item => item.checked),
      timeRange: this.currentRange !== 'All' ? this.currentRange : null,
      staff: this.staff
    };
    return filter;
  }

  changeSearch(searchValue) {
    this.searchInputSubject.next(searchValue);
  }

  changeSearchStaff(searchValue) {
    this.searchStaffSubject.next(searchValue);
  }

  clearFilters() {
    this.searchStaffInput.nativeElement.value = '';
    this.searchStaffSubject.next('');
    this.callTypeButtonTitle = 'All';
    this.callTypes.forEach(item => item.checked = false);
    this.currentRange = 'All';
    this.filterChange.emit(this.getFilter());
  }
}
