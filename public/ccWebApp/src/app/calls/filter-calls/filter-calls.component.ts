import { CallsService } from './../../services/calls/calls.service';
import { Subscription } from 'rxjs/Subscription';
import { CallsTotal } from './../../models/calls-total';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-filter-calls',
  templateUrl: './filter-calls.component.html',
  styleUrls: ['./filter-calls.component.scss']
})
export class FilterCallsComponent implements OnInit, OnDestroy {
  callsTotal: CallsTotal = <CallsTotal>{};
  private callsTotalSubscription: Subscription;
  constructor(private callsService: CallsService) { }

  ngOnInit() {
    this.callsTotalSubscription = this.callsService.getOpenCallsTotal()
      .subscribe(data => {
        this.callsTotal = data;
      });
  }

  ngOnDestroy() {
    this.callsTotalSubscription.unsubscribe();
  }

  changeSearch(value) {
    this.callsService.setSearchInput(value);
  }
}
