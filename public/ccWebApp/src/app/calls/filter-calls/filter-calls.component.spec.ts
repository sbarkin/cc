import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterCallsComponent } from './filter-calls.component';

describe('FilterCallsComponent', () => {
  let component: FilterCallsComponent;
  let fixture: ComponentFixture<FilterCallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterCallsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterCallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
