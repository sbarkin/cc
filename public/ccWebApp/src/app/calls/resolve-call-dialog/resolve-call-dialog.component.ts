import { CallsService } from './../../services/calls/calls.service';
import { Call } from './../../models/call';
import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-resolve-call-dialog',
  templateUrl: './resolve-call-dialog.component.html',
  styleUrls: ['./resolve-call-dialog.component.scss']
})
export class ResolveCallDialogComponent implements OnInit {
  call: Call;
  onClose: any;
  form: FormGroup;
  constructor(public modalRef: BsModalRef,
    private callsService: CallsService) {
    this.form = new FormGroup({
      actionId: new FormControl('2',  [
        Validators.required
      ]),
      note: new FormControl(),
    });
    this.form.controls.actionId.valueChanges.subscribe((newValue) => {
      if (newValue === '4' || newValue === '3') {
        this.form.controls.note.setValidators([Validators.required]);
      } else {
        this.form.controls.note.clearValidators();
      }
      this.form.controls.note.updateValueAndValidity();
    });
  }

  ngOnInit() {
  }

  resolveCall() {
    console.log('submit', this.form.value);
    this.form.get('note').markAsDirty();
    if (this.form.valid) {
      this.callsService.resolveCall(this.call.id, this.form.value.actionId, this.form.value.note)
        .subscribe(result => {
          // this.activeModal.close(result);
          this.onClose(result);
          // this.modalRef.hide();
        });
    }
  }

}
