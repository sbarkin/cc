import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolveCallDialogComponent } from './resolve-call-dialog.component';

describe('ResolveCallComponent', () => {
  let component: ResolveCallDialogComponent;
  let fixture: ComponentFixture<ResolveCallDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolveCallDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolveCallDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
