import { OpenCallsGridComponent } from './open-calls-grid/open-calls-grid.component';
import { ResolvedCallsComponent } from './resolved-calls/resolved-calls.component';
import { ContainerComponent } from './container/container.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OpenCallsComponent } from './open-calls/open-calls.component';

const routes: Routes = [
    {
      path: '',
      component: ContainerComponent,
      children: [
        { path: '', pathMatch: 'full', redirectTo: 'open' },
        {
          path: 'open',
          component: OpenCallsComponent,
          children: [
            { path: '', pathMatch: 'full', redirectTo: 'all' },
            { path: ':filterType', component: OpenCallsGridComponent}
          ]
        },
        {
          path: 'resolved',
          component: ResolvedCallsComponent
        }
      ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallsRoutingModule { }
