import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MomentModule } from 'angular2-moment';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { CallsRoutingModule } from './calls-routing.module';
import { ContainerComponent } from './container/container.component';
import { OpenCallsComponent } from './open-calls/open-calls.component';
import { ResolvedCallsComponent } from './resolved-calls/resolved-calls.component';
import { FilterCallsComponent } from './filter-calls/filter-calls.component';
import { OpenCallsGridComponent } from './open-calls-grid/open-calls-grid.component';
import { ResolveCallDialogComponent } from './resolve-call-dialog/resolve-call-dialog.component';
import { FilterResolvedCallsComponent } from './filter-resolved-calls/filter-resolved-calls.component';
import { SharedModule } from '../shared/shared.module';
import { PopoverModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    CallsRoutingModule,
    SharedModule,
    MomentModule,
    ReactiveFormsModule,
    FormsModule,
    PopoverModule.forRoot(),
    NgbModule,
    InfiniteScrollModule
  ],
  declarations: [
    ContainerComponent,
    OpenCallsComponent,
    ResolvedCallsComponent,
    FilterCallsComponent,
    OpenCallsGridComponent,
    ResolveCallDialogComponent,
    FilterResolvedCallsComponent
  ],
  entryComponents: [
    ResolveCallDialogComponent
  ]
})
export class CallsModule { }
