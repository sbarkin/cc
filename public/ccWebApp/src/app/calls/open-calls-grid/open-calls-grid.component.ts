import { DialogParams } from './../../models/dialog-params';
import { DialogService } from './../../services/dialog/dialog.service';
import { CallsTotal } from './../../models/calls-total';
import { SocketService } from './../../services/socket/socket.service';
import { CallRequestParams } from './../../models/call-request-params';
import { CallsService } from './../../services/calls/calls.service';
import { Subscription } from 'rxjs/Subscription';
import { Call } from './../../models/call';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap  } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import * as moment from 'moment';
// import {NgbModal, NgbActiveModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
import { ResolveCallDialogComponent } from '../resolve-call-dialog/resolve-call-dialog.component';

const sevenMinutes = 7;
const periodOfTimer = 5000;

@Component({
  selector: 'app-open-calls-grid',
  templateUrl: './open-calls-grid.component.html',
  styleUrls: ['./open-calls-grid.component.scss']
})
export class OpenCallsGridComponent implements OnInit, OnDestroy {
  calls: Array<Call> = [];
  sortOrder = 'asc';
  sortBy = 'room';
  callType: string;
  searchText: string;
  callsTotal: CallsTotal = <CallsTotal>{};
  offset = 0;
  private callsTotalSubscription: Subscription;
  private routeSubscription: Subscription;
  private searchSubscription: Subscription;
  private updateCallsSubscription: Subscription;
  private watcherTimer;
  constructor(private route: ActivatedRoute,
    private callsService: CallsService,
    private socketService: SocketService,
    // private modalService: NgbModal,
    private modalService: BsModalService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.routeSubscription = this.route.paramMap
      .switchMap((params: ParamMap) => {
        return Observable.of({filterType: params.get('filterType')});
      }).subscribe(data => {
        this.callType = data.filterType;
        this.offset = 0;
        this.getCalls().subscribe(calls => this.handlerCallsData(calls));
      });

    this.searchSubscription = this.callsService.getSearchInput().pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term => {
        this.searchText = term;
        this.offset = 0;
        return this.getCalls();
      })
    ).subscribe(calls => this.handlerCallsData(calls));

    this.updateCallsSubscription = this.socketService.getMessage('updateCall').pipe(
      debounceTime(300),
      switchMap(() => {
        this.offset = 0;
        return this.getCalls();
      })
    ).subscribe(calls => this.handlerCallsData(calls));

    this.callsTotalSubscription = this.callsService.getOpenCallsTotal()
    .subscribe(data => {
      this.callsTotal = data;
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
    this.searchSubscription.unsubscribe();
    this.updateCallsSubscription.unsubscribe();
  }

  setSortBy(sortBy: string) {
    if (this.sortBy === sortBy) {
      this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortBy = sortBy;
      this.sortOrder = 'asc';
    }
    this.offset = 0;
    this.getCalls().subscribe(result => this.handlerCallsData(result));
  }

  resolveCall(call: Call) {
    // const modalRef = this.modalService.open(ResolveCallDialogComponent, { centered: true });
    const modalRef = this.modalService.show(ResolveCallDialogComponent, {
      initialState: {
        call
      },
      class: 'modal-dialog-centered'
    });
    modalRef.content.onClose = (result) => {
      modalRef.hide();
      if (result) {
        this.dialogService.openDialog(<DialogParams>{
          type: 'success',
          title: 'Success!',
          message: 'Call has been resolved'
        });
      }
    };
    // modalRef.componentInstance.call = call;
    // modalRef.result.then((result) => {
    //   if (result) {
    //     this.dialogService.openDialog('success', 'Success!', 'Call has been resolved');
    //   }
    // }, (reason) => null);
  }

  onScroll() {
    this.offset += 20;
    this.getCalls().subscribe();
  }

  private getCalls(): Observable<[Call]> {
    const params = <CallRequestParams>{};
    if (this.callType !== 'all') {
      params.type = this.callType;
    }
    params['sort-by'] = this.sortBy;
    params['sort-order'] = this.sortOrder;
    if (this.searchText) {
      params['q'] = this.searchText;
    }
    return this.callsService.loadCalls('open', params);
  }

  private handlerCallsData(result) {
    const data = result.rows;
    data.forEach(item => item.stopWatcherClass = this.getStopWatcherClass(item));
    if (this.offset) {
      this.calls = this.calls.concat(data);
    } else {
      this.calls = data;
    }
    this.checkLapsedTime();
  }

  private checkLapsedTime() {
    clearTimeout(this.watcherTimer);
    this.calls.forEach(item => {
        if (item.stopWatcherClass === 'stopwatch-amber' && this.isSevenMinutesPassed(item.date)) {
          item.stopWatcherClass = 'stopwatch-red';
        }
    });
    if (this.calls.filter(item => item.stopWatcherClass === 'stopwatch-amber').length) {
      this.watcherTimer = setTimeout(() => this.checkLapsedTime(), periodOfTimer);
    }
  }

  private getStopWatcherClass(call: Call) {
    if (this.isSevenMinutesPassed(call.date)) {
      return 'stopwatch-red';
    } else {
      return 'stopwatch-amber';
    }
  }

  private isSevenMinutesPassed(unixtime: number): boolean {
    return Math.abs(moment().diff(moment.unix(unixtime), 'minute')) >= sevenMinutes ? true : false;
  }

}
