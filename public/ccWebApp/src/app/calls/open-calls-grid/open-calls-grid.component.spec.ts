import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenCallsGridComponent } from './open-calls-grid.component';

describe('OpenCallsGridComponent', () => {
  let component: OpenCallsGridComponent;
  let fixture: ComponentFixture<OpenCallsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenCallsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenCallsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
