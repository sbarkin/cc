import { UserService } from './../../services/user/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { UserData } from '../../models/user-data';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit, OnDestroy {
  userDataSubscription: Subscription;
  userData: UserData;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userDataSubscription = this.userService.getUserData()
      .subscribe((data: UserData) => this.userData = data);
  }

  ngOnDestroy() {
    this.userDataSubscription.unsubscribe();
  }
}
