import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {

  constructor(private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const ccUserData = JSON.parse(localStorage.getItem('ccUser'));
    let headers = req.headers;
    if (ccUserData && ccUserData.token) {
      headers = req.headers.set('x-access-token', ccUserData.token)
        .set('Cache-Control', 'no-cache')
        .set('Pragma', 'no-cache')
        .set('Expires', 'Sat, 01 Jan 2000 00:00:00 GMT')
        .set('If-Modified-Since', '0');
    }
    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({headers});

    // send cloned request with header to the next handler.
    return next.handle(authReq).catch((err: HttpErrorResponse) => {
        const isResetPasswordLink = /api\/auth\/reset-password\//.test(err.url);
        if (err.status === 401 && ['/login', '/forgot-password'].indexOf(this.router.url.split(';')[0]) === -1
            && !isResetPasswordLink ) {
          this.router.navigate(['/login']);
        }
        return Observable.throw(err);
      }
    );
  }
}
