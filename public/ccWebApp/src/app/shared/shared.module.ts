import { DialogService } from './../services/dialog/dialog.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { SortOrderComponent } from './sort-order/sort-order.component';
import { HandlePopoverCloseDirective } from './close-popover/close-popover.directive';

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot()
  ],
  declarations: [SortOrderComponent, HandlePopoverCloseDirective ],
  exports: [
    SortOrderComponent, HandlePopoverCloseDirective
  ],
  providers: [
    DialogService
  ]
})
export class SharedModule { }
