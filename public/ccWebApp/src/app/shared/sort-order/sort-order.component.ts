import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sort-order',
  templateUrl: './sort-order.component.html',
  styleUrls: ['./sort-order.component.scss']
})
export class SortOrderComponent {
  @Input() sortOrder: string;
  constructor() {}

}
