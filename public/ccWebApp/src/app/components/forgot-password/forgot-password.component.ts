import { UserService } from './../../services/user/user.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ParamMap, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {
  private routeSubscription: Subscription;
  form: FormGroup;
  errorRequest = false;
  errorMessage = 'Please check your username and try again';
  inProcess = false;
  isRequestSubmitted = false;
  submitResult = '';
  submitMessage = '';
  constructor(private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) {
    this.form = new FormGroup({
      name: new FormControl('',  [
        Validators.required
      ])
    });
  }

  ngOnInit() {
    this.routeSubscription = this.route.params.subscribe((params: ParamMap) => {
        const email = params['email'];
        if (email) {
          this.form.setValue({
            name: email
          });
        }
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  submitForm() {
    this.errorRequest = false;
    this.form.get('name').markAsDirty();
    if (this.form.valid) {
      this.inProcess = true;
      this.userService.forgotPassword(this.form.value.name)
        .subscribe((result: any) => {
          this.inProcess = false;
          console.log(result);
          if (result.success) {
            this.isRequestSubmitted = true;
            this.submitResult = result.result;
            this.submitMessage = result.message;
          } else {
            this.setErrorRequest();
          }
        },
        error => {
          this.inProcess = false;
          console.log('error forgot password:', error);
          this.setErrorRequest();
        }
      );
    }
  }

  private setErrorRequest() {
    this.form.get('name').setErrors({'incorrect': true});
    this.errorRequest = true;
    this.form.setErrors({'incorrect': true});
  }

  goBack() {
    this.router.navigate(['/login']);
  }
}
