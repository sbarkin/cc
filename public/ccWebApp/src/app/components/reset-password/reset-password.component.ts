import { Staff } from './../../models/staff';
import { UserService } from './../../services/user/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  form: FormGroup;
  inProcess = false;
  isSuccess = false;
  user: Staff;
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.form = new FormGroup({
      password: new FormControl('',  [
        Validators.required
      ])
    });
  }

  ngOnInit() {
    this.route.data
      .subscribe((data: {user: Staff}) => {
        this.user = data.user;
      });
  }

  submitForm() {
    this.form.get('password').markAsDirty();
    if (this.form.valid) {
      this.inProcess = true;
      this.user.password = this.form.value.password;
      this.userService.updatePassword(this.user)
        .subscribe((result: any) => {
          this.inProcess = false;
          if (result) {
            this.isSuccess = result;
          } else {
            // try reload page if error update password
            location.reload();
          }
        },
        error => {
          this.inProcess = false;
          console.log('error reset password:', error);
        }
      );
    }
  }

  goForgotPassword() {
    this.router.navigate(['/forgot-password']);
  }

  goLogin() {
    this.router.navigate(['/login']);
  }
}
