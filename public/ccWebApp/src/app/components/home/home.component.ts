import { ServiceWorkerService } from './../../services/service-worker/service-worker.service';
import { Subscription } from 'rxjs/Subscription';
import { UserData } from './../../models/user-data';
import { DialogService } from './../../services/dialog/dialog.service';
import { DialogParams } from './../../models/dialog-params';
import { UserService } from './../../services/user/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  currentUser: UserData;
  userDataSubscription: Subscription;
  constructor(
    private userService: UserService,
    private dialogService: DialogService,
    private swService: ServiceWorkerService
  ) { }

  ngOnInit() {
    this.userDataSubscription = this.userService.getUserData().subscribe((data: UserData) => {
      this.currentUser = data;
    });
    // subscribing to web-push notification
    this.swService.subscribeToPush();
  }

  ngOnDestroy() {
    this.userDataSubscription.unsubscribe();
  }

  logout() {
    const dialogRef = this.dialogService.openDialog(<DialogParams>{
      type: 'warning',
      message:  'Are you sure that you want logout?',
      buttonCancelTitle: 'Cancel',
      buttonOkTitle: `Yes, logout`
    });
    dialogRef.content.onButtonCancelClick = () => {
      dialogRef.hide();
    };
    dialogRef.content.onButtonOkClick = () => {
      dialogRef.hide();
      this.swService.unsubscribeFromPush()
        .subscribe(res => {
          this.userService.logout();
        }, err => {
          console.log('error unsibscribe:', err);
        });

    };
    return false;
  }
}
