import { UserService } from './../../services/user/user.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  errorRequest: Boolean = false;
  errorMessage: String = '';
  inProcess: Boolean = false;
  constructor(private userService: UserService, private router: Router) {
    this.form = new FormGroup({
      name: new FormControl('',  [
        Validators.required
      ]),
      password: new FormControl('',  [
        Validators.required
      ]),
    });
  }

  ngOnInit() {
  }

  submitForm() {
    this.errorRequest = false;
    this.form.get('name').markAsDirty();
    this.form.get('password').markAsDirty();
    if (this.form.valid) {
      this.inProcess = true;
      this.userService.login(this.form.value.name, this.form.value.password)
        .subscribe(result => {
          this.inProcess = false;
          console.log(result);
          if (result.success) {
            this.router.navigate(['/calls']);
          } else {
            this.setErrorRequest(result.message);
          }
        },
        error => {
          this.inProcess = false;
          console.log('error login:', error);
          this.setErrorRequest('Username failed.');
        }
      );
    }
  }

  setErrorRequest(errorType) {
    let message = 'Please check your username and try again';
    if (errorType === 'Password failed.') {
      message = 'Please check your password and try again';
      this.form.get('password').setErrors({'incorrect': true});
    } else {
      this.form.get('name').setErrors({'incorrect': true});
    }
    this.errorMessage = message;
    this.errorRequest = true;
    this.form.setErrors({'incorrect': true});
  }

  forgotPassword() {
    if (this.form.value.name) {
      this.router.navigate(['/forgot-password', { email: this.form.value.name }]);
    } else {
      this.router.navigate(['/forgot-password']);
    }
  }

  requestAccess() {
    this.router.navigate(['/request-access']);
  }
}
