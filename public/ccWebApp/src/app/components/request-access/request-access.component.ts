import { Router } from '@angular/router';
import { UserService } from './../../services/user/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request-access',
  templateUrl: './request-access.component.html',
  styleUrls: ['./request-access.component.scss']
})
export class RequestAccessComponent implements OnInit {
  form: FormGroup;
  isRequestSubmitted = false;
  inProcess: Boolean = false;
  errorRequest = false;
  constructor(private userService: UserService, private router: Router) {
    this.form = new FormGroup({
      name: new FormControl('',  [
        Validators.required
      ]),
      email: new FormControl('',  [
        Validators.required,
        Validators.pattern(/^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.{1,1}[a-z]{2,4})$/)
      ]),
    });
   }

  ngOnInit() {
  }

  submitForm() {
    this.form.get('name').markAsDirty();
    this.form.get('email').markAsDirty();
    this.errorRequest = false;
    if (this.form.valid) {
      this.inProcess = true;
      this.userService.requestAccess(this.form.value.name, this.form.value.email)
        .subscribe(result => {
          this.inProcess = false;
          if (result.success) {
            this.isRequestSubmitted = true;
          }
        },
        error => {
          this.inProcess = false;
          console.log('error login:', error);
        }
      );
    } else {
      this.errorRequest = true;
    }
  }

  goBack() {
    this.router.navigate(['/login']);
  }
}
