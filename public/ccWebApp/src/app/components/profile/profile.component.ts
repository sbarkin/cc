import { Staff } from './../../models/staff';
import { UserData } from './../../models/user-data';
import { UserService } from './../../services/user/user.service';
import { StaffService } from './../../services/staff/staff.service';
import { UserRolesService } from './../../services/user-roles/user-roles.service';
import { UserRoles } from './../../models/user-roles';
import { DialogService } from './../../services/dialog/dialog.service';
import { DialogParams } from './../../models/dialog-params';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  form: FormGroup;
  userRoles: Array<UserRoles> = [];
  userData: UserData = <UserData>{};
  userDataSubscription: Subscription;
  inProcess = false;
  constructor(private userRolesService: UserRolesService,
    private userService: UserService,
    private dialogService: DialogService,
    private location: Location
  ) {
    this.form = new FormGroup({
      first_name: new FormControl('',  [
        Validators.required
      ]),
      last_name: new FormControl('',  [
        Validators.required
      ]),
      email: new FormControl('',  [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('',  [
      ]),
      role: new FormControl('',  [
        Validators.required
      ])
    });
  }

  ngOnInit() {
    this.userDataSubscription = this.userService.getUserData()
      .subscribe((data: UserData) => {
        this.userData = data;
        if (this.userData.user) {
          this.form.patchValue({
            first_name: this.userData.user.first_name,
            last_name: this.userData.user.last_name,
            email: this.userData.user.email,
            role: this.userData.user.user_roles_id
          });
          if (this.userData.user.user_roles_id !== 1) {
            this.form.disable();
          }
        }
      });
    this.userRolesService.getUserRoles().subscribe(data => {
      this.userRoles = data;
    });
  }

  submitForm() {
    Object.keys( this.form.controls).forEach(key => {
      this.form.controls[key].markAsDirty();
    });
    if (this.form.valid) {
      this.inProcess = true;
      this.userService.editUser(<Staff> {
        id: this.userData.userId,
        first_name: this.form.value.first_name,
        last_name: this.form.value.last_name,
        email: this.form.value.email,
        password: this.form.value.password,
        user_roles_id: this.form.value.role
      }).subscribe(result => {
        this.inProcess = false;
        if (result) {
          this.dialogService.openDialog(<DialogParams>{
            type: 'success',
            title: 'Success!',
            message: `Your profile has been updated`
          });
        }
      });
    }
  }

  goBack() {
    this.location.back();
  }  

  ngOnDestroy() {
    this.userDataSubscription.unsubscribe();
  }

}
