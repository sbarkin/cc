import { StaffService } from './../../services/staff/staff.service';
import { UserRoles } from './../../models/user-roles';
import { UserRolesService } from './../../services/user-roles/user-roles.service';
import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Staff } from '../../models/staff';

@Component({
  selector: 'app-staff-dialog',
  templateUrl: './staff-dialog.component.html',
  styleUrls: ['./staff-dialog.component.scss']
})
export class StaffDialogComponent implements OnInit {
  action = 'new';
  onClose: any;
  onActive: any;
  onInactive: any;
  onRemove: any;
  form: FormGroup;
  userRoles: Array<UserRoles> = [];
  staff: Staff = <Staff>{};
  constructor(public modalRef: BsModalRef,
    private userRolesService: UserRolesService,
    private staffService: StaffService) {
    this.form = new FormGroup({
      first_name: new FormControl('',  [
        Validators.required
      ]),
      last_name: new FormControl('',  [
        Validators.required
      ]),
      email: new FormControl('',  [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('',  [
      ]),
      role: new FormControl('',  [
        Validators.required
      ])
    });
  }

  ngOnInit() {
    this.userRolesService.getUserRoles().subscribe(data => {
      this.userRoles = data;
    });
    if (this.action === 'edit') {
      this.form.patchValue({
        first_name: this.staff.first_name,
        last_name: this.staff.last_name,
        email: this.staff.email,
        role: this.staff.user_roles_id
      });
    }
    if (this.action === 'new') {
      this.form.controls.password.setValidators([Validators.required]);
    }
    this.form.controls.password.updateValueAndValidity();
  }

  addStaff() {
    Object.keys( this.form.controls).forEach(key => {
      this.form.controls[key].markAsDirty();
    });
    console.log(this.form.value);
    if (this.form.valid) {
      this.staffService.newStaff(<Staff> {
        first_name: this.form.value.first_name,
        last_name: this.form.value.last_name,
        email: this.form.value.email,
        password: this.form.value.password,
        user_roles_id: this.form.value.role
      }).subscribe(result => {
        this.onClose(result);
      });
    }
  }

  editStaff() {
    Object.keys( this.form.controls).forEach(key => {
      this.form.controls[key].markAsDirty();
    });
    if (this.form.valid) {
      this.staffService.editStaff(<Staff> {
        id: this.staff.id,
        first_name: this.form.value.first_name,
        last_name: this.form.value.last_name,
        email: this.form.value.email,
        password: this.form.value.password,
        user_roles_id: this.form.value.role
      }).subscribe(result => {
        this.onClose(result);
      });
    }
  }

}
