import { Patient } from './../../models/patient';
import { PatientsService } from './../../services/patients/patients.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-patient-dialog',
  templateUrl: './patient-dialog.component.html',
  styleUrls: ['./patient-dialog.component.scss']
})
export class PatientDialogComponent implements OnInit {
  action = 'new';
  patient: Patient = <Patient>{};
  onClose: any;
  onDischarge: any;
  onAdmit: any;
  onRemove: any;
  form: FormGroup;
  constructor(public modalRef: BsModalRef,
    private patientService: PatientsService
  ) {
    this.form = new FormGroup({
      first_name: new FormControl('',  [
        Validators.required
      ]),
      last_name: new FormControl('',  [
        Validators.required
      ]),
      last_four_sn: new FormControl('',  [
        Validators.required,
        Validators.pattern('[0-9]{4}')
      ]),
      room_number: new FormControl('',  [
        Validators.required
      ])
    });
  }

  ngOnInit() {
    if (this.action === 'edit') {
      this.form.patchValue({
        first_name: this.patient.first_name,
        last_name: this.patient.last_name,
        room_number: this.patient.room_number,
        last_four_sn: this.patient.last_four_sn
      });
    }
  }

  addPatient() {
    Object.keys( this.form.controls).forEach(key => {
      this.form.controls[key].markAsDirty();
    });
    if (this.form.valid) {
      this.patientService.newPatient(<Patient> {
        first_name: this.form.value.first_name,
        last_name: this.form.value.last_name,
        room_number: this.form.value.room_number,
        last_four_sn: this.form.value.last_four_sn
      }).subscribe(result => {
        this.onClose(result);
      });
    }
  }

  editPatient() {
    Object.keys( this.form.controls).forEach(key => {
      this.form.controls[key].markAsDirty();
    });
    if (this.form.valid) {
      this.patientService.editPatient(<Patient> {
        id: this.patient.id,
        first_name: this.form.value.first_name,
        last_name: this.form.value.last_name,
        room_number: this.form.value.room_number,
        last_four_sn: this.form.value.last_four_sn
      }).subscribe(result => {
        this.onClose(result);
      });
    }
  }
}
