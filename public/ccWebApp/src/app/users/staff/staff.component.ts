import { SocketService } from './../../services/socket/socket.service';
import { DialogService } from './../../services/dialog/dialog.service';
import { DialogParams } from './../../models/dialog-params';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { StaffService } from './../../services/staff/staff.service';
import { Staff } from './../../models/staff';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { StaffRequestParams } from '../../models/staff-request-params';
import { map, catchError } from 'rxjs/operators';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
import { StaffDialogComponent } from '../staff-dialog/staff-dialog.component';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.scss']
})
export class StaffComponent implements OnInit, OnDestroy {
  searchInput = new Subject<string>();
  searchInputSubscription: Subscription;
  updateUsersSubscription: Subscription;
  users: Array<Staff> = [];
  usersState = 'active';
  usersRole = 'all';
  sortOrder = 'desc';
  sortBy = 'user';
  offset = 0;
  searchText = '';
  usersNumber = 0;
  constructor(private staffService: StaffService,
    private modalService: BsModalService,
    private socketService: SocketService,
    private dialogService: DialogService) { }

  ngOnInit() {
    this.searchInputSubscription = this.searchInput.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term => {
        this.searchText = term;
        this.offset = 0;
        return this.getUsers();
      })
    ).subscribe();

    this.updateUsersSubscription = this.socketService.getMessage('updateUsers').pipe(
      debounceTime(300),
      switchMap(() => {
        this.offset = 0;
        return this.getUsers();
      })
    ).subscribe();

    this.getUsers().subscribe();
  }

  ngOnDestroy() {
    this.searchInputSubscription.unsubscribe();
    this.updateUsersSubscription.unsubscribe();
  }

  private getUsers(): Observable<boolean> {
    const params = <StaffRequestParams>{};
    if (this.usersRole !== 'all') {
      params['role'] = this.usersRole;
    }
    params['sort-by'] = this.sortBy;
    params['sort-order'] = this.sortOrder;
    params['offset'] = this.offset;
    if (this.searchText) {
      params['q'] = this.searchText;
    }
    return this.staffService.loadStaff(this.usersState, params)
      .pipe(
        map((result: any) => {
          if (this.offset) {
            this.users = this.users.concat(result.rows);
          } else {
            this.users = result.rows;
          }
          this.usersNumber = result.count;
          return true;
        })
      );
  }

  setFilter(filter) {
    this.usersState = filter.state.toLowerCase();
    if (filter.roles.length) {
      this.usersRole = filter.roles.map(item => item.name.toLowerCase()).join();
     } else {
      this.usersRole = 'all';
     }

    this.offset = 0;
    this.getUsers().subscribe();
  }

  setSortBy(sortBy: string) {
    if (this.sortBy === sortBy) {
      this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortBy = sortBy;
      this.sortOrder = 'asc';
    }
    this.offset = 0;
    this.getUsers().subscribe();
  }

  onScroll() {
    this.offset += 20;
    this.getUsers().subscribe();
  }

  newStaff() {
    const modalRef: BsModalRef = this.modalService.show(StaffDialogComponent, {
      initialState: {
        action: 'new'
      },
      class: 'modal-dialog-centered'
    });
    modalRef.content.onClose = (result) => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        if (result) {
          this.dialogService.openDialog(<DialogParams>{
            type: 'success',
            title: 'Success!',
            message: 'New User has been added'
          });
        }
      });
    };
  }

  editStaff(staff) {
    const modalRef: BsModalRef = this.modalService.show(StaffDialogComponent, {
      initialState: {
        action: 'edit',
        staff
      },
      class: 'modal-dialog-centered'
    });
    modalRef.content.onClose = (result) => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        if (result) {
          this.dialogService.openDialog(<DialogParams>{
            type: 'success',
            title: 'Success!',
            message: `${staff.user_name} has been updated`
          });
        }
      });
    };
    modalRef.content.onInactive = () => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.setStatus('inactive', staff);
      });
    };
    modalRef.content.onActive = () => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.setStatus('active', staff);
      });
    };
    modalRef.content.onRemove = () => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.removeStaff(staff);
      });

    };
  }

  setStatus(status: string, staff: Staff) {
    let status_id;
    let message;
    let statusTitle;
    if (status === 'inactive') {
      status_id = 2;
      message = `Are you sure that you want to make ${staff.user_name} inactive?`;
      statusTitle = 'Make Inactive';
    } else if (status === 'active') {
      status_id = 1;
      message = `Are you sure that you want to make ${staff.user_name} active?`;
      statusTitle = 'Make Active';
    } else if (status === 'remove') {
      status_id = 3;
      message = `Are you sure that you want to remove ${staff.user_name}?`;
      statusTitle = 'remove';
    } else {
      console.log(`error: unknown status ${status}`);
      return false;
    }
    const dialogRef: BsModalRef = this.dialogService.openDialog(<DialogParams>{
      type: 'warning',
      message,
      buttonCancelTitle: 'Cancel',
      buttonOkTitle: `Yes, ${statusTitle}`
    });
    dialogRef.content.onButtonCancelClick = () => {
      dialogRef.hide();
    };
    dialogRef.content.onButtonOkClick = () => {
      dialogRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.updateStatus(staff, status_id).subscribe();
      });
    };
  }

  private removeStaff(staff: Staff) {
    this.staffService.getTotalActions(staff.id)
    .subscribe(result => {
      if (result && result.total) {
          this.displayCantRemoveDialog(staff);
      } else {
        this.setStatus('remove', staff);
      }
    });
  }

  private updateStatus(staff: Staff, status_id: number): Observable<boolean> {
    let statusDone = '';
    if (status_id === 2) {
      statusDone = 'made inactive';
    } else if (status_id === 1) {
      status_id = 1;
      statusDone = 'made active';
    } else if (status_id === 3) {
      status_id = 3;
      statusDone = 'removed';
    } else {
      console.log(`error: unknown status ${status_id}`);
    }
    return this.staffService.editStaff(<Staff> {
      id: staff.id,
      user_statuses_id: status_id
    }).pipe(
      map(result => {
        if (result) {
          this.dialogService.openDialog(<DialogParams>{
            type: 'success',
            title: 'Success!',
            message: `${staff.user_name} has been ${statusDone}`
          });
        }
        return true;
      }),
      catchError(err => {
        console.log('catch error on edit patient:', err);
        return Observable.of(false);
      })
    );
  }

  private displayCantRemoveDialog(staff) {
    const dialogRef: BsModalRef = this.dialogService.openDialog(<DialogParams>{
      type: 'warning',
      subTitle: `Can't remove ${staff.user_name}`,
      subMessage: 'Staff has records associated with it and can not be removed. Make Inactive instead?',
      buttonCancelTitle: 'Cancel',
      buttonOkTitle: 'Make Inactive'
    });
    dialogRef.content.onButtonCancelClick = () => {
      dialogRef.hide();
    };
    dialogRef.content.onButtonOkClick = () => {
      dialogRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.updateStatus(staff, 2).subscribe();
      });
    };
  }

}
