import { DialogParams } from './../../models/dialog-params';
import { SocketService } from './../../services/socket/socket.service';
import { DialogService } from './../../services/dialog/dialog.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { PatientsService } from './../../services/patients/patients.service';
import { Observable } from 'rxjs/Observable';
import { Patient } from './../../models/patient';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PatientsRequestParams } from '../../models/patients-request-params';
import { map, catchError } from 'rxjs/operators';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
import { PatientDialogComponent } from '../patient-dialog/patient-dialog.component';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit, OnDestroy {
  searchInput = new Subject<string>();
  searchInputSubscription: Subscription;
  updatePatientsSubscription: Subscription;
  patients: Array<Patient> = [];
  patientState = 'active';
  sortOrder = 'desc';
  sortBy = 'patient';
  offset = 0;
  searchText = '';
  patientsNumber = 0;
  constructor(private patientsService: PatientsService,
    private dialogService: DialogService,
    private modalService: BsModalService,
    private patientService: PatientsService,
    private socketService: SocketService ) {
  }

  ngOnInit() {
    this.searchInputSubscription = this.searchInput.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term => {
        this.searchText = term;
        this.offset = 0;
        return this.getPatients();
      })
    ).subscribe();

    this.updatePatientsSubscription = this.socketService.getMessage('updatePatients').pipe(
      debounceTime(300),
      switchMap(() => {
        this.offset = 0;
        return this.getPatients();
      })
    ).subscribe();

    this.getPatients().subscribe();
  }

  ngOnDestroy() {
    this.searchInputSubscription.unsubscribe();
    this.updatePatientsSubscription.unsubscribe();
  }

  private getPatients(): Observable<boolean> {
    const params = <PatientsRequestParams>{};
    params['sort-by'] = this.sortBy;
    params['sort-order'] = this.sortOrder;
    params['offset'] = this.offset;
    if (this.searchText) {
      params['q'] = this.searchText;
    }
    return this.patientsService.loadPatients(this.patientState, params)
      .pipe(
        map((result: any) => {
          if (this.offset) {
            this.patients = this.patients.concat(result.rows);
          } else {
            this.patients = result.rows;
          }
          this.patientsNumber = result.count;
          return true;
        })
      );
  }

  setFilter(state) {
    this.patientState = state.toLowerCase();

    this.offset = 0;
    this.getPatients().subscribe();
  }

  setSortBy(sortBy: string) {
    if (this.sortBy === sortBy) {
      this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortBy = sortBy;
      this.sortOrder = 'asc';
    }
    this.offset = 0;
    this.getPatients().subscribe();
  }

  onScroll() {
    this.offset += 20;
    this.getPatients().subscribe();
  }

  newPatient() {
    const modalRef = this.modalService.show(PatientDialogComponent, {
      initialState: {
        action: 'new'
      },
      class: 'modal-dialog-centered'
    });
    modalRef.content.onClose = (result) => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        if (result) {
          this.dialogService.openDialog(<DialogParams>{
            type: 'success',
            title: 'Success!',
            message: 'New Patient has been added'
          });
        }
      });
    };
  }

  editPatient(patient: Patient) {
    const modalRef = this.modalService.show(PatientDialogComponent, {
      initialState: {
        action: 'edit',
        patient
      },
      class: 'modal-dialog-centered'
    });
    modalRef.content.onClose = (result) => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        if (result) {
          this.dialogService.openDialog(<DialogParams>{
            type: 'success',
            title: 'Success!',
            message: 'Patient has been updated'
          });
        }
      });
    };

    modalRef.content.onDischarge = () => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.setAction('discharge', patient);
      });
    };
    modalRef.content.onAdmit = () => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.setAction('admit', patient);
      });

    };
    modalRef.content.onRemove = () => {
      modalRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.removePatient(patient);
      });
    };
  }

  setAction(action: string, patient: Patient) {
    let action_id;
    if (action === 'discharge') {
      action_id = 2;
    } else if (action === 'admit') {
      action_id = 1;
    } else if (action === 'remove') {
      action_id = 3;
    } else {
      console.log(`error: unknown action ${action}`);
      return false;
    }
    const dialogRef = this.dialogService.openDialog(<DialogParams>{
      type: 'warning',
      message: `Are you sure that you want to ${action} ${patient.patient_name}?`,
      buttonCancelTitle: 'Cancel',
      buttonOkTitle: `Yes, ${action}`
    });
    dialogRef.content.onButtonCancelClick = () => {
      dialogRef.hide();
    };
    dialogRef.content.onButtonOkClick = () => {
      dialogRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.updateAction(patient, action_id).subscribe();
      });
    };
  }

  private removePatient(patient: Patient) {
    this.patientService.getPatientTotalCalls(patient.id)
      .subscribe(result => {
        if (result && result.total) {
          this.displayCantRemoveDialog(patient);
        } else {
          this.setAction('remove', patient);
        }
      });
  }

  private updateAction(patient: Patient, action_id: number): Observable<boolean> {
    let actionDone;
    if (action_id === 1) {
      actionDone = 'readmitted';
    } else if (action_id === 2) {
      actionDone = 'discharged';
    } else if (action_id === 3) {
      actionDone = 'removed';
    } else {
      console.log(`error: unknown action ${action_id}`);
    }
    return this.patientService.editPatient(<Patient> {
      id: patient.id,
      patient_actions_id: action_id
    }).pipe(
      map(result => {
        if (result) {
          this.dialogService.openDialog(<DialogParams>{
            type: 'success',
            title: 'Success!',
            message: `${patient.patient_name} has been ${actionDone}`
          });
        }
        return true;
      }),
      catchError(err => {
        console.log('catch error on edit patient:', err);
        return Observable.of(false);
      })
    );
  }

  private displayCantRemoveDialog(patient) {
    const dialogRef = this.dialogService.openDialog(<DialogParams>{
      type: 'warning',
      subTitle: `Can't remove ${patient.patient_name}`,
      subMessage: 'Patient has records associated with it and can not be removed. Discharge instead?',
      buttonCancelTitle: 'Cancel',
      buttonOkTitle: 'Discharge'
    });
    dialogRef.content.onButtonCancelClick = () => {
      dialogRef.hide();
    };
    dialogRef.content.onButtonOkClick = () => {
      dialogRef.hide();
      const modalHiddenSubscription: Subscription = this.modalService.onHidden.subscribe(() => {
        modalHiddenSubscription.unsubscribe();
        this.updateAction(patient, 2).subscribe();
      });
    };
  }
}
