import { StaffComponent } from './staff/staff.component';
import { PatientsComponent } from './patients/patients.component';
import { ContainerComponent } from './container/container.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: '',
  component: ContainerComponent,
  children: [
    { path: '', pathMatch: 'full', redirectTo: 'patients' },
    {
      path: 'patients',
      component: PatientsComponent
    },
    {
      path: 'staff',
      component: StaffComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
