import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterStaffComponent } from './filter-staff.component';

describe('FilterStaffComponent', () => {
  let component: FilterStaffComponent;
  let fixture: ComponentFixture<FilterStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
