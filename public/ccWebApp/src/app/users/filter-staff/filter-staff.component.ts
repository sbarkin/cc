import { UserRolesService } from './../../services/user-roles/user-roles.service';
import { UserRoles } from './../../models/user-roles';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter-staff',
  templateUrl: './filter-staff.component.html',
  styleUrls: ['./filter-staff.component.scss']
})
export class FilterStaffComponent implements OnInit {
  userRoles: Array<UserRoles> = [];
  roleButtonTitle = 'All';
  statusButtonTitle = 'Active';
  private currentFilter = {
    roles: [],
    state: this.statusButtonTitle
  };
  @Input() usersNumber = 0;
  @Input() searchInputSubject: Subject<string>;
  @Output() filterChange = new EventEmitter();
  @Output() newStaff = new EventEmitter();
  constructor(private userRolesService: UserRolesService) { }

  ngOnInit() {
    this.userRolesService.getUserRoles().subscribe(data => {
      this.userRoles = data;
    });
  }

  roleChange() {
    const userRoles = this.userRoles.filter(item => item.checked);
    if (userRoles.length === 0) {
      this.roleButtonTitle = 'All';
    } else if (userRoles.length === 1) {
      this.roleButtonTitle = userRoles[0].name;
    } else {
      this.roleButtonTitle = 'Multiple';
    }
    this.currentFilter.roles = userRoles;
    this.filterChange.emit(this.currentFilter);
  }

  setStatus(isChecked) {
    console.log('set status', isChecked);
    this.statusButtonTitle = isChecked ? 'All' : 'Active';
    this.currentFilter.state = this.statusButtonTitle.toLowerCase();
    this.filterChange.emit(this.currentFilter);
  }

  changeSearch(searchValue) {
    this.searchInputSubject.next(searchValue);
  }

  onNewStaff() {
    this.newStaff.emit();
  }
}
