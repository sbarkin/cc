import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { UsersRoutingModule } from './users-routing.module';
import { ContainerComponent } from './container/container.component';
import { PatientsComponent } from './patients/patients.component';
import { StaffComponent } from './staff/staff.component';
import { FilterPatientsComponent } from './filter-patients/filter-patients.component';
import { SharedModule } from '../shared/shared.module';
import { FilterStaffComponent } from './filter-staff/filter-staff.component';
import { PatientDialogComponent } from './patient-dialog/patient-dialog.component';
import { StaffDialogComponent } from './staff-dialog/staff-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    InfiniteScrollModule
  ],
  declarations: [
    ContainerComponent,
    PatientsComponent,
    StaffComponent,
    FilterPatientsComponent,
    FilterStaffComponent,
    PatientDialogComponent,
    StaffDialogComponent
  ],
  entryComponents: [
    PatientDialogComponent,
    StaffDialogComponent
  ]
})
export class UsersModule { }
