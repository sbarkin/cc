import { Subject } from 'rxjs/Subject';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter-patients',
  templateUrl: './filter-patients.component.html',
  styleUrls: ['./filter-patients.component.scss']
})
export class FilterPatientsComponent implements OnInit {
  statusButtonTitle = 'Active';
  @Input() patientsNumber = 0;
  @Input() searchInputSubject: Subject<string>;
  @Output() filterChange = new EventEmitter();
  @Output() newPatient = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  setStatus(isChecked) {
    this.statusButtonTitle = isChecked ? 'All' : 'Active';
    this.filterChange.emit(this.statusButtonTitle.toLowerCase());
  }

  changeSearch(searchValue) {
    this.searchInputSubject.next(searchValue);
  }

  onNewPatient() {
    this.newPatient.emit();
  }
}
