export interface UserRoles {
  id: number;
  name: string;
  checked: boolean;
}
