export interface CallTypes {
  id: number;
  name: string;
  short_name: string;
  icon_name: string;
  title: string;
  subtitle: string;
  checked: boolean;
}
