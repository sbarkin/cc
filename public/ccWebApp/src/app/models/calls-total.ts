export interface CallsTotal {
  all: number;
  pain: number;
  bathroom: number;
  drink: number;
  tvRemote: number;
  question: number;
}
