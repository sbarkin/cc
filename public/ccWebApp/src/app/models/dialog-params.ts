export interface DialogParams {
  type: string;
  title: string;
  subTitle: string;
  message: string;
  subMessage: string;
  buttonCancelTitle: string;
  buttonOkTitle: string;
}
