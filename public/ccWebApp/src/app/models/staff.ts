export interface Staff {
  id: number;
  user_name: string;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  user_roles_id: number;
  user_statuses_id: number;
  user_status: {
    id: number;
    name: string;
  };
  user_role: {
    id: number;
    name: string;
  };
  reset_password_token: string;
}
