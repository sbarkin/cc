export interface CallRequestParams {
  'type': string;
  'sort-by': string;
  'sort-order': string;
  'range': string;
  'q': string;
  'q-staff': string;
}
