export interface PatientsRequestParams {
  'sort-by': string;
  'sort-order': string;
  'q': string;
}
