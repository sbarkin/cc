export interface Patient {
  id: number;
  patient_name: string;
  first_name: string;
  last_name: string;
  last_four_sn: string;
  room_number: string;
  patient_action: {
    id: number;
    name: string;
  };
  patient_actions_id?: number;
}
