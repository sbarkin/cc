export interface UserData {
  token: string;
  userId: number;
  user: {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    user_roles_id: number;
    user_statuses_id: number;
  };
}
