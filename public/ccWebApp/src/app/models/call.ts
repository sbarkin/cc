export interface Call {
  id: number;
  date: number;
  patient_id: number;
  call_types_id: number;
  call_actions_id: number;
  note: string;
  action_by_users_id: number;
  action_date: number;
  response_time: number;
  patient: {
    room_number: string;
    patient_name: string;
  };
  call_type: {
    id: number;
    name: string;
    short_name: string;
    icon_name: string;
    title: string;
  };
  call_action: {
    id: number;
    name: string;
  };
  user: {
    user_name: string;
  };
  stopWatcherClass: string;
}
