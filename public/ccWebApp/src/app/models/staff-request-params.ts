export interface StaffRequestParams {
  'role': string;
  'sort-by': string;
  'sort-order': string;
  'q': string;
}
