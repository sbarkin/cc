import { ServiceWorkerService } from './services/service-worker/service-worker.service';
import { SocketService } from './services/socket/socket.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'CommuniCare';
  constructor(private socketService: SocketService,
    private serviceWorkerService: ServiceWorkerService
  ) { }

  ngOnInit() {
    this.socketService.startSocketClient();
    this.serviceWorkerService.initSW();
  }

}
