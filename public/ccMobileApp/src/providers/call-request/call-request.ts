import { CallInterface } from './../../models/call-interface';
import { ApiRequestProvider } from './../api-request/api-request';
import { Injectable } from '@angular/core';

/*
  Generated class for the CallRequestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CallRequestProvider {

  constructor(private api: ApiRequestProvider) {
    console.log('Hello CallRequestProvider Provider');
  }

  makeCallrequest(callTypeId: number) {
    return this.api.request('POST', 'v1/patient/{patientId}/call', {
      call_types_id: callTypeId
    });
  }

  getLastCall() {
    return this.api.request('GET', 'v1/patient/{patientId}/call/last')
      .then((response: any) => {
        if (response.success && response.result) {
          return <CallInterface> response.result;
        } else {
          return <CallInterface> {};
        }
      });
  }

  cancelCallRequest(callId: number) {
    return this.api.request('DELETE', `v1/patient/{patientId}/call/${callId}`)
      .then(result => {
        return true;
      })
      .catch(err => {
        if (err.status === 405) {
          return false;
        } else {
          throw err;
        }
      });
  }
}
