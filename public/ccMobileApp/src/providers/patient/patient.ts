import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { apiUrl } from './../config';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { PatientInterface } from './../../models/patient-interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Events } from 'ionic-angular';
/*
  Generated class for the PatientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PatientProvider {
  private token: string;
  private userId: number;
  private patient: PatientInterface = <PatientInterface> {};
  private patientSubject = new BehaviorSubject<PatientInterface>(<PatientInterface> {})
  constructor(public http: HttpClient,
    private storage: Storage,
    private events: Events
  ) {
    console.log('Hello AuthProvider Provider');
  }

  getMessage() {
    return this.patientSubject.asObservable();
  }

  getUserFromStorageAndCheckAuth() {
    return this.storage.get('patientData')
      .then(result => {
        let data = JSON.parse(result);
        if (data && data.patient) {
          const name = data.patient.first_name + ' ' + data.patient.last_name[0];
          return this.checkAuth(name, data.patient.last_four_sn);
        } else {
          return false;
        }
      });
  }

  checkAuth(name: string, lastFourSn: number) {
    return this.getToken(name, lastFourSn)
      .then((result: any) => {
          this.token = result.token;
          this.userId = result.userId;
          return this.getPatient();
      }).then((result: any) => {
        this.patient = result;
        this.patientSubject.next(result);
        this.savePatientData();
        return true;
      }).catch(err => {
        this.clearPatientData();
        return false;
      });
  }

  logout(): Promise<any> {
    return this.clearPatientData().then(() => {
      this.events.publish('user:logout');
    });
  }

  private clearPatientData() {
    this.token = null;
    this.userId = null;
    this.patient = <PatientInterface> {};
    this.patientSubject.next(this.patient);
    return this.storage.clear();
  }

  private getPatient() {
    const authUrl = `${apiUrl}/v1/patient/${this.userId}`;
    let headers: HttpHeaders = new HttpHeaders({
      'x-access-token': this.token
    });
    return this.http.get(authUrl, {headers}).toPromise();
  }

  private savePatientData() {
    const patientData = {
      token: this.token,
      userId: this.userId,
      patient: this.patient
    };
    this.storage.set('patientData', JSON.stringify(patientData));
  }

  private getToken(name: string, lastFourSn: number) {
    const authUrl = `${apiUrl}/auth/patient` ;
    const data = {
      name: name,
      last_four_sn: lastFourSn
    };
    return this.http.post(authUrl, data).toPromise();
  }
}
