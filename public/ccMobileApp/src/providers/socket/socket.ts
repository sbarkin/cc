import { Observable } from 'rxjs/Observable';
import { PatientInterface } from './../../models/patient-interface';
import { PatientProvider } from './../patient/patient';
import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';

/*
  Generated class for the SocketProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SocketProvider {
  private patientSubscription;
  constructor(private socket: Socket,
    private patientProvider: PatientProvider
  ) {
    console.log('Hello SocketProvider Provider');
    this.socket.on('connect', () => {
      this.patientSubscription = this.patientProvider.getMessage().subscribe((patient: PatientInterface) => {
        this.socket.emit('setPatient', patient);
      });
    });
    this.socket.on('disconnect', () => {
      this.patientSubscription.unsubscribe();
    })
  }

  getMessage(msgName: string): Observable<Object> {
    return this.socket
        .fromEvent(msgName);
  }
}
