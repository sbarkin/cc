import { ApiRequestProvider } from './../api-request/api-request';
import { Injectable } from '@angular/core';

/*
  Generated class for the CallTypesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CallTypesProvider {

  constructor(private api: ApiRequestProvider) {
    console.log('Hello CallTypesProvider Provider');
  }

  getCallTypes() {
    return this.api.request('GET', 'v1/call-types');
  }

}
