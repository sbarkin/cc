import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Events } from 'ionic-angular';
import { apiUrl } from './../config';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiRequestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiRequestProvider {

  constructor(public http: HttpClient,
    private events: Events,
    private storage: Storage) {
    console.log('Hello ApiRequestProvider Provider');
  }

  request(typeRequest: string, apiPoint: string, body: object = {}) {
    return this.storage.get('patientData')
      .then(result => {
        let data = JSON.parse(result);
        if (data && data.token && data.userId) {
          apiPoint = apiPoint.replace('{patientId}', data.userId);
          const authUrl = `${apiUrl}/${apiPoint}`;
          let headers: HttpHeaders = new HttpHeaders({
            'x-access-token': data.token
          });
          return this.http.request(typeRequest, authUrl, {body, headers}).toPromise()
          .catch(err => {
            if (err.status && err.status === 401) {
              this.events.publish('user:logout');
            } else {
              throw err;
            }
          });
        } else {
          this.events.publish('user:logout');
          return false;
        }
      });
  }

}
