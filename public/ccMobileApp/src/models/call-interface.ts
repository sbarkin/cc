export interface CallInterface {
  id: number;
  date: string;
  patients_id: number;
  call_types_id: number;
  call_actions_id: number;
  note: string;
  action_by_users_id: number;
  action_date: string;
}
