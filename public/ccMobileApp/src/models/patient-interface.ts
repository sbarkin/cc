export interface PatientInterface {
  id: number;
  units_id: number;
  first_name: string;
  last_name: string;
  last_four_sn: number;
  room_number: string;
  patient_actions_id: number;
}
