import { PatientProvider } from './../../providers/patient/patient';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Validators, FormControl,FormGroup } from '@angular/forms';
import { Events } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  form: FormGroup;
  errorRequest: boolean = false;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private events: Events,
    private loadingCtrl: LoadingController,
    private patientProvider: PatientProvider) {
    this.form = new FormGroup({
      name: new FormControl('',  [
        Validators.required
      ]),
      lastFourSn: new FormControl('',  [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(4),
        Validators.pattern(/\d{4}/)
      ]),
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  submitForm() {
    this.errorRequest = false;
    this.form.get('name').markAsDirty()
    this.form.get('lastFourSn').markAsDirty();
    if (this.form.valid) {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present();
      this.patientProvider.checkAuth(this.form.value.name, this.form.value.lastFourSn)
        .then(result => {
          loading.dismiss();
          if (result) {
            this.events.publish('user:login');
          } else {
            this.setErrorRequest();
          }
        }).catch(err => {
          console.log('err', err);
          loading.dismiss();
          this.setErrorRequest();
        })
    }
  }

  setErrorRequest() {
    this.errorRequest = true;
    this.form.setErrors({'incorrect': true})
  }
}
