import { callActionId } from './../../providers/config';
import { CallInterface } from './../../models/call-interface';
import { CallRequestProvider } from './../../providers/call-request/call-request';
import { ToastProvider } from './../../providers/toast/toast';
import { CallTypesInterface } from './../../models/callTypes-interface';
import { CallTypesProvider } from './../../providers/call-types/call-types';
import { PatientProvider } from './../../providers/patient/patient';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { PatientInterface } from '../../models/patient-interface';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  patient: PatientInterface = <PatientInterface> {};
  callTypes: Array<CallTypesInterface> = [];
  lastCall: CallInterface;
  patientSubscription: Subscription;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private patientProvider: PatientProvider,
    private callTypesProvider: CallTypesProvider,
    private callRequestProvider: CallRequestProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toast: ToastProvider
  ) {
    this.patientSubscription = this.patientProvider.getMessage().subscribe((patient: PatientInterface) => {
      this.patient = patient;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.callTypesProvider.getCallTypes()
      .then((data: Array<CallTypesInterface>) => {
        this.callTypes = data;
        return this.callRequestProvider.getLastCall();
      }).then((call: CallInterface) => {
        this.lastCall = call;
        loading.dismiss();
        this.checkLastCall();
      }).catch(err => {
        loading.dismiss();
        this.toast.showToast('Sorry, we couldn\'t get data. Please check connect to server');
      });

  }

  checkLastCall() {
    if (this.lastCall && this.lastCall.call_actions_id === callActionId) {
      const callType = this.callTypes.filter(item => item.id === this.lastCall.call_types_id);
      if (callType.length) {
        this.goToCallRequestPage(callType[0], this.lastCall.id);
      }
    }
  }

  ionViewCanEnter(): boolean {
    console.log('ion view can enter Home page');
    if (!this.patient || !this.patient.id) {
      this.patientSubscription.unsubscribe();
      return false;
    } else {
      if (this.lastCall) {
        this.callRequestProvider.getLastCall()
          .then(call => {
            this.lastCall = call;
            this.checkLastCall();
          }).catch(err => {
            console.log('error', err);
            this.toast.showToast('Sorry, we couldn\'t get data. Please check connect to server');
          });
      }
      return true;
    }
  }

  callRequest(callType: CallTypesInterface) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.callRequestProvider.makeCallrequest(callType.id).then((result: any) => {
      loading.dismiss();
      if (result.success && result.id) {
        this.goToCallRequestPage(callType, result.id);
      } else {
        throw Error('Error create call request:' + JSON.stringify(result));
      }
    }).catch(err => {
      loading.dismiss();
      console.log('Error call request', err);
      this.toast.showToast('Sorry, we couldn\'t make call request. Please check connect to server and try again');
    });

  }

  goToCallRequestPage(callType: CallTypesInterface, callId: number) {
    this.navCtrl.push('CallRequestPage',{callType, patient: this.patient, callId}, {
      animate: true,
      animation: 'ios-transition'
    });
  }

  logoutDialog() {
    let alert = this.alertCtrl.create({
      title: 'Logout',
      message: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.logout();
          }
        }
      ]
    });
    alert.setMode('ios');
    alert.present();
  }

  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.patientProvider.logout()
      .then(() => loading.dismiss())
      .catch(err => loading.dismiss());
  }

}
