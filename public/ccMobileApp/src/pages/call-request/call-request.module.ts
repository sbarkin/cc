import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CallRequestPage } from './call-request';


@NgModule({
  declarations: [
    CallRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(CallRequestPage)
  ],
})
export class CallRequestPageModule {}
