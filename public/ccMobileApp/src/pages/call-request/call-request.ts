import { SocketProvider } from './../../providers/socket/socket';
import { PatientProvider } from './../../providers/patient/patient';
import { ToastProvider } from './../../providers/toast/toast';
import { CallRequestProvider } from './../../providers/call-request/call-request';
import { CallTypesInterface } from './../../models/callTypes-interface';
import { PatientInterface } from './../../models/patient-interface';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

/**
 * Generated class for the CallRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-call-request',
  templateUrl: 'call-request.html',
})
export class CallRequestPage {
  patient: PatientInterface = <PatientInterface> {};
  callType: CallTypesInterface = <CallTypesInterface> {};
  callId: number;
  isCallRequest: boolean;
  resolveCallSubscription: Subscription;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private callRequestProvider: CallRequestProvider,
    private patientProvider: PatientProvider,
    private toast: ToastProvider,
    private loadingCtrl: LoadingController,
    private socketProvider: SocketProvider
  ) {
    this.callType = this.navParams.get('callType');
    this.patient = this.navParams.get('patient');
    this.callId = this.navParams.get('callId');
    this.isCallRequest = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CallRequestPage');
    this.resolveCallSubscription = this.socketProvider.getMessage('resolveCall').
      subscribe((message) => {
        this.toast.showToast('Call was resolved.');
        this.isCallRequest = false;
        this.navCtrl.pop();
      });
  }

  ionViewDidLeave(){
    this.resolveCallSubscription.unsubscribe();
  }

  ionViewCanEnter(): boolean {
    if (!this.callType || !this.patient || !this.callId) {
      return false;
    } else {
      return true;
    }
  }

  ionViewCanLeave(): boolean {
    return !this.isCallRequest;
  }

  cancelCallDialog() {
    let alert = this.alertCtrl.create({
      title: 'Cancel Call?',
      message: 'Are you sure you want to cancel the call to nurse?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.cancelCall();
          }
        }
      ]
    });
    alert.setMode('ios');
    alert.present();
  }

  cancelCall() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.callRequestProvider.cancelCallRequest(this.callId)
      .then(result => {
        loading.dismiss();
        this.isCallRequest = false;
        this.navCtrl.pop();
      }).catch(err => {
        loading.dismiss();
        console.log('Error call request', err);
        this.toast.showToast('Sorry, we couldn\'t make cancel request. Please check connect to server and try again');
      });
  }

  logoutDialog() {
    let alert = this.alertCtrl.create({
      title: 'Logout',
      message: 'Are you sure you want to cancel your call to the nurse and logout?',
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.logout();
          }
        }
      ]
    });
    alert.setMode('ios');
    alert.present();
  }

  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.callRequestProvider.cancelCallRequest(this.callId)
      .then(result => {
        this.isCallRequest = false;
        return this.patientProvider.logout();
      })
      .then(() => loading.dismiss())
      .catch(err => {
        loading.dismiss();
        console.log('Error cancel request request and logout', err);
        this.toast.showToast('Sorry, we couldn\'t make cancel request and logout. Please check connect to server and try again');
      });
  }
}
