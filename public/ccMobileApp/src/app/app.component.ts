import { PatientInterface } from './../models/patient-interface';
import { SocketProvider } from './../providers/socket/socket';
import { PatientProvider } from './../providers/patient/patient';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, LoadingController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = '';

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private events: Events,
    private loadingCtrl: LoadingController,
    private patientProvider: PatientProvider,
    private socketProvider: SocketProvider,
    private alertCtrl: AlertController
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.subscribeEvents();
      this.checkUser();
    });
  }

  subscribeEvents() {
    this.events.subscribe('user:login', () => {
      this.nav.setRoot('HomePage');
    });
    this.events.subscribe('user:logout', () => {
      this.nav.setRoot('LoginPage');
    });
    this.socketProvider.getMessage('updateCalls').subscribe();
    this.socketProvider.getMessage('updatePatient').subscribe((data: PatientInterface) => {
      const logoutTimeout = 10000;
      if (data && (data.patient_actions_id === 2 || data.patient_actions_id === 3)) {
        const actionName = data.patient_actions_id === 2 ? 'discharged': 'removed';

        let alertTimeout;

        let alert = this.alertCtrl.create({
          title: actionName.toUpperCase(),
          message: `You have been ${actionName} :)`,
          buttons: [
            {
              text: 'Logout',
              handler: () => {
                clearTimeout(alertTimeout);
                this.logoutAction();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        alert.setMode('ios');
        alert.present();

        alertTimeout = setTimeout(() => {
          alert.dismiss();
          this.logoutAction();
        }, logoutTimeout);

      }
    });
  }

  checkUser() {
    const loading = this.loadingCtrl.create({
      content: 'Please wait, loading app...'
    });
    loading.present();
    this.patientProvider.getUserFromStorageAndCheckAuth()
      .then(result => {
        loading.dismiss();
        if (result) {
          this.rootPage = 'HomePage';
        } else {
          this.rootPage = 'LoginPage';
        }
      }).catch(err => {
        loading.dismiss();
        this.rootPage = 'LoginPage';
      });
  }

  private logoutAction() {
    if (this.nav.getActive().id === 'CallRequestPage') {
      this.nav.getActive().instance.isCallRequest = false;
    }
    this.patientProvider.logout().catch(err => {
      console.log('error when logout:', err);
    });
  }

}

