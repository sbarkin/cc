import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';

import { MyApp } from './app.component';
import { PatientProvider } from '../providers/patient/patient';
import { CallTypesProvider } from '../providers/call-types/call-types';
import { ApiRequestProvider } from '../providers/api-request/api-request';
import { ToastProvider } from '../providers/toast/toast';
import { CallRequestProvider } from '../providers/call-request/call-request';
import { SocketProvider } from '../providers/socket/socket';

import { socketIoUrl } from '../providers/config';
const socketIoConfig: SocketIoConfig = { url: socketIoUrl, options: {} };

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(socketIoConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PatientProvider,
    CallTypesProvider,
    ApiRequestProvider,
    SocketProvider,
    ToastProvider,
    CallRequestProvider
  ]
})
export class AppModule {}
