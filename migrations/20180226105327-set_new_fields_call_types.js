'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.createTable('users', { id: Sequelize.INTEGER });
        */
        queryInterface.sequelize.import('../models/call_types');
        return queryInterface.sequelize.model('call_types').update({
            'icon_name': 'headache.svg',
            'title': 'Pain',
            'subtitle': "I'm in"
        }, {
            where: {
                id: 1
            }
        }).then(() => {
            return queryInterface.sequelize.model('call_types').update({
                'icon_name': 'toilet.svg',
                'title': 'Bathroom',
                'subtitle': "I need the"
            }, {
                where: {
                    id: 2
                }
            })
        }).then(() => {
            return queryInterface.sequelize.model('call_types').update({
                'icon_name': 'glass-water.svg',
                'title': 'Drink',
                'subtitle': "I need a"
            }, {
                where: {
                    id: 3
                }
            })
        }).then(() => {
            return queryInterface.sequelize.model('call_types').update({
                'icon_name': 'television.svg',
                'title': 'TV/Remove',
                'subtitle': "I have trouble with my"
            }, {
                where: {
                    id: 4
                }
            })
        }).then(() => {
            return queryInterface.sequelize.model('call_types').update({
                'icon_name': 'help.svg',
                'title': 'Question/Concern',
                'subtitle': "I have a "
            }, {
                where: {
                    id: 5
                }
            })
        })
    },

    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.dropTable('users');
        */
    }
};