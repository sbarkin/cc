'use strict';
var crypto = require('crypto');
var commonService = require('./../lib/common.service');

module.exports = {
    up: (queryInterface, Sequelize) => {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.createTable('users', { id: Sequelize.INTEGER });
        */
        queryInterface.sequelize.import('../models/users');
        const password = '123456';
        return commonService.setHash(password)
            .then(hash => {
                return queryInterface.sequelize.model('users').create({
                    first_name: 'Admin',
                    last_name: 'Test',
                    email: 'svolodko@gmail.com',
                    password: hash,
                    user_roles_id: 1,
                    user_statuses_id: 1
                });
            })

    },

    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.dropTable('users');
        */
    }
};