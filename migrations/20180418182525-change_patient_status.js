'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.createTable('users', { id: Sequelize.INTEGER });
        */
        queryInterface.sequelize.import('../models/patient_actions');
        return queryInterface.sequelize.model('patient_actions').update({
            'name': 'Active'
        }, {
            where: {
                id: 1
            }
        }).then(() => {
            return queryInterface.sequelize.model('patient_actions').update({
                'name': 'Discharged'
            }, {
                where: {
                    id: 2
                }
            });
        });
    },

    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.dropTable('users');
        */
    }
};