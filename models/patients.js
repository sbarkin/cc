/* jshint indent: 2 */

/**
 * @swagger
 * definitions:
 *   Patient:
 *     type: object
 *     required:
 *       - unit_id
 *       - first_name
 *       - last_name
 *       - last_four_sn
 *       - room_number
 *       - patient_actions_id
 *     properties:
 *       id:
 *         type: number
 *       unit_id:
 *         type: number
 *       first_name:
 *         type: string
 *       last_name: 
 *         type: string
 *       last_four_sn:
 *         type: string
 *       room_number:
 *         type: string
 *       patient_actions_id:
 *         type: number
 *   Patients:
 *     type: array
 *     items:
 *       $ref: '#/definitions/Patient'
 */
module.exports = function(sequelize, DataTypes) {
    const Patients = sequelize.define('patients', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        units_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'units',
                key: 'id'
            }
        },
        first_name: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        last_name: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        last_four_sn: {
            type: DataTypes.STRING(4),
            allowNull: true
        },
        room_number: {
            type: DataTypes.STRING(25),
            allowNull: true
        },
        patient_actions_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'patient_actions',
                key: 'id'
            }
        }
    }, {
        tableName: 'patients',
        timestamps: false
    });

    Patients.associate = models => {
        Patients.belongsTo(models.patient_actions, { foreignKey: 'patient_actions_id' });
    }
    return Patients;
};