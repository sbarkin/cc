module.exports = function(sequelize, DataTypes) {
    const UserSubscriptions = sequelize.define('user_subscriptions', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        users_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'users',
                key: 'id'
            }
        },
        endpoint: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        auth: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        p256dh: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        expired: {
            type: DataTypes.BIGINT(15),
            allowNull: true,
        }
    }, {
        tableName: 'user_subscriptions',
        timestamps: false
    });

    return UserSubscriptions;
};