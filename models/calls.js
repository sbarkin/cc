/* jshint indent: 2 */

/**
 * @swagger
 * definitions:
 *   Call:
 *     type: object
 *     required:
 *       - date
 *       - call_type_id
 *     properties:
 *       id:
 *         type: number
 *       date:
 *         type: number
 *         description: unixtimestamp of date create request
 *       patient_id:
 *         type: number
 *       call_types_id:
 *         type: number
 *       call_actions_id:
 *         type: number
 *       note:
 *         type: string
 *       action_by_users_id:
 *         type: number
 *       action_date:
 *         type: string,
 *       patient:
 *         type: object
 *         properties: 
 *          room_number: 
 *              type: string
 *          patient_name: 
 *              type: string
 *       call_type:
 *              $ref: '#/definitions/CallType'
 *   Calls:
 *     type: array
 *     items:
 *       $ref: '#/definitions/Call'
 */
module.exports = function(sequelize, DataTypes) {
    const Calls = sequelize.define('calls', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        date: {
            type: DataTypes.DATE,
            allowNull: true
        },
        patients_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'patients',
                key: 'id'
            }
        },
        call_types_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'call_types',
                key: 'id'
            }
        },
        call_actions_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'call_actions',
                key: 'id'
            }
        },
        note: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        action_by_users_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'users',
                key: 'id'
            }
        },
        action_date: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'calls',
        timestamps: false
    });

    Calls.associate = models => {
        Calls.belongsTo(models.patients, { foreignKey: 'patients_id' });
        Calls.belongsTo(models.call_types, { foreignKey: 'call_types_id' });
        Calls.belongsTo(models.call_actions, { foreignKey: 'call_actions_id' });
        Calls.belongsTo(models.users, { foreignKey: 'action_by_users_id' });
    }

    return Calls;
};