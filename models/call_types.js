/* jshint indent: 2 */
/**
 * @swagger
 * definitions:
 *   CallType:
 *     type: object
 *     required:
 *       - name
 *       - short_name
 *     properties:
 *       id:
 *         type: number
 *       name:
 *         type: string
 *       short_name:
 *         type: string
 *       icon:
 *         type: string
 *       title:
 *         type: string
 *       subtitle:
 *         type: string
 *   CallTypes:
 *     type: array
 *     items:
 *       $ref: '#/definitions/CallType'
 */
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('call_types', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        short_name: {
            type: DataTypes.STRING(25),
            allowNull: true
        },
        icon_name: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        title: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        subtitle: {
            type: DataTypes.STRING(50),
            allowNull: true
        }
    }, {
        tableName: 'call_types',
        timestamps: false
    });
};