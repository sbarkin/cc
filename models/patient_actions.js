/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('patient_actions', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: true
        }
    }, {
        tableName: 'patient_actions',
        timestamps: false
    });
};