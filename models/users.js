/* jshint indent: 2 */

/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     required:
 *       - id
 *       - first_name
 *       - last_name
 *       - email
 *       - user_roles_id
 *       - user_statuses_id
 *     properties:
 *       id:
 *         type: number
 *       first_name:
 *         type: string
 *       last_name: 
 *         type: string
 *       email:
 *         type: string
 *       user_roles_id:
 *         type: number
 *       user_statuses_id:
 *         type: number
 *   Users:
 *     type: array
 *     items:
 *       $ref: '#/definitions/User'
 */
module.exports = function(sequelize, DataTypes) {
    const Users = sequelize.define('users', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        first_name: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        last_name: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        email: {
            type: DataTypes.STRING(50),
            allowNull: true
        },
        password: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        user_roles_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'user_roles',
                key: 'id'
            }
        },
        user_statuses_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'user_statuses',
                key: 'id'
            }
        },
        reset_password_token: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        reset_password_expired: {
            type: DataTypes.BIGINT(15),
            allowNull: true,
        }
    }, {
        tableName: 'users',
        timestamps: false
    });

    Users.associate = models => {
        Users.belongsTo(models.user_statuses, { foreignKey: 'user_statuses_id' });
        Users.belongsTo(models.user_roles, { foreignKey: 'user_roles_id' });
    }

    return Users;
};