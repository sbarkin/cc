/* jshint indent: 2 */
/**
 * @swagger
 * definitions:
 *   UserRole:
 *     type: object
 *     required:
 *       - name
 *     properties:
 *       id:
 *         type: number
 *       name:
 *         type: string
 *   UserRoles:
 *     type: array
 *     items:
 *       $ref: '#/definitions/UserRole'
 */
module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user_roles', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(50),
            allowNull: true
        }
    }, {
        tableName: 'user_roles',
        timestamps: false
    });
};