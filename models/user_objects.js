/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user_objects', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        users_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'users',
                key: 'id'
            }
        },
        facility_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'facility',
                key: 'id'
            }
        },
        units_id: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            references: {
                model: 'units',
                key: 'id'
            }
        }
    }, {
        tableName: 'user_objects',
        timestamps: false
    });
};