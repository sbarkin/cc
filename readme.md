# Welcome to CommuniCare project!

## Arhitecture

The components which comprise the software architecture include:

- Api server designed by node.js v8.10.0
    Api server include next modules:
    - Api REST server module designed with [express v4](http://expressjs.com/). 

        Api use JSON Web Token (JWT) for representing claims securely between two parties.

        Api Interactive Documentation designed with swagger can be found [there](http://159.89.180.31:3000/api-docs/)

    - Socket Server module designed with [socket.io v2](https://socket.io/)

    - Web push service module designed with [web-push v3](https://github.com/web-push-libs/web-push/)

    Working with Communicare DB carried out with use [Sequelize ORM](http://docs.sequelizejs.com/)

- Web application designed with [Angular v5.2](https://angular.io)

    It consist of App, Calls, Users and Admin modules. 
    
    Web app used service workers if browser supports it.

- Mobile application designed with [Ionic framework v3](https://ionicframework.com/)

    It consist of Home, Call Request and Login components.


## Database Design
Software use MySql Database Server.

This is [Schema DB](install/schema_db.png)

## Installation

### Prerequisites
Make sure you have installed all of the following prerequisites on your machine:

- Node.js v8 [Download & Install Node.js](https://nodejs.org/en/download/)
- MySQL [Download & Install MySQL](http://www.mysql.com/downloads)
- Angular CLI
    ```
    npm install -g @angular/cli
    ```
- The Ionic and Cordova CLI.
    ```
    npm install -g ionic cordova
    ```
- Apache HTTP server if you are going to use it as http server for web app

### Install Server App

#### 1. Cloning The GitHub Repository
```
git clone git@bitbucket.org:sbarkin/cc.git
cd cc
```
#### 2. Install Node.js dependencies 
```
npm install
```
#### 3. Configure Server App
Copy config/config.default.json to config/config.json

Configuration file can support several configuration environment. As default used "development" environment. So edit *config.development* object of config.json

Description *config.development* object:

*auth* - used for signing JWT token

*db* - parameters for connecting to DB

*swagger.baseName* - base name for swagger, api documentation will be able by link config.swagger.baseName + ':' + config.apiPort

*apiPort* - api server port

*socketIoPort* - socket.io server port

*mailer* - node-mailer parameters

*webApp.baseHref* - href for web app

*webPush* - parameters for web push notification

#### 4. Deploy dump of DB
- Create Db with name as set in *config.db.database*
- Using mysql command, deploy dump from install/communicare_dump.sql to created DB

#### 5. Creating user with admin role
Run in console:
```
node install/create_admin_user adminFirstName adminLastName adminEmail adminPassword
```
For example, create admin user Peter Pen with email pp@pp.com and password 123456:
```
node install/create_admin_user Peter Pen pp@pp.com 123456
```
#### 6. Run server
Api server is run by command:
```
node app
```
You can use [pm2](http://pm2.keymetrics.io/docs/usage/quick-start/) process manager for autostart app

After start you can try open Api Docs in browser http://server:port/api-docs,
 where *server* is your server address, *port* is parameter from config.apiPort.

### Install Web App 

#### 1. Go to Web App directory
Web app is placed id directory *public/ccWebApp*, so run in console:
```
cd public/ccWebApp
```

#### 2. Install Node.js dependencies
```
npm install
```
#### 3. Setup app config

Copy src/app/services/config.default.ts to src/app/services/config.ts

Change *apiUrl* and *SocketioUrl* parameters according to your server setting

Parameter *webPushPublicKey* used for web push notification and correspond to *webPush.vapidKey.publicKey* parameter of server config

#### 4. Fix service worker for notification click event support

If you want to respond "notificationclick" event (when you click the notification, the browser will pop up and open the specified URL), do next:

- open in editor node_modules/@angular/service-worker/ngsw-worker.js file;

- add the codes below to ngsw-worker.js around the line 
    ```
    this.scope.addEventListener('push', (event) => this.onPush(event));(Line 1775).
    ```

    ```
                    this.scope.addEventListener('notificationclick', (event) => {
                        console.log('[Service Worker] Notification click Received. event:%s', event);
                        event.notification.close();
                        if (clients.openWindow && event.notification.data.url) {
                            event.waitUntil(
                                clients.matchAll({
                                    type: "window"
                                })
                                .then(function(clientList) {
                                    for (var i = 0; i < clientList.length; i++) {
                                        var client = clientList[i];
                                        if (client.url == event.notification.data.url && 'focus' in client)
                                            return client.focus();
                                    }
                                    if (clients.openWindow) {
                                        return clients.openWindow(event.notification.data.url);
                                    }
                                })
                            );
                        }
                    });    
    ```
#### 5. Build Web App
 Run command:
```
    ng build --prod --output-path=/wwwDirectory
```
where *wwwDirectory* - path to directory your www server

If you will use Apache as www server you can use this [.htaccess](install/.htaccess) for correct work relative links of app

Now you can login to web app with email and password that you created on step 5 of install Server App.

### Install Mobile App 

#### 1. Go to Mobile App directory
Mob app is placed id directory public/ccMobileApp, so run in console:
```
cd public/ccMobileApp
```

#### 2. Install Node.js dependencies
```
npm install
```
#### 3. Setup app config

Copy src/providers/config.default.ts to src/providers/config.ts

Change *apiUrl* and *SocketioUrl* parameters according to your server setting

#### 4. Build App for Web

```
npm run ionic:build --prod
```
After run it you got code for web-server in folder "www" 

#### 5. Deploying to Andoid and IOS Devices

Run steps as described [this](https://ionicframework.com/docs/intro/deploying) for deploying application to mobile devices

For Android devices please see also this [description](public/ccMobileApp/build-android-apk.txt) 