-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 03 2018 г., 15:07
-- Версия сервера: 5.6.31
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `communicare_empty`
--

-- --------------------------------------------------------

--
-- Структура таблицы `calls`
--

CREATE TABLE IF NOT EXISTS `calls` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `patients_id` int(11) DEFAULT NULL,
  `call_types_id` int(11) DEFAULT NULL,
  `call_actions_id` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `action_by_users_id` int(11) DEFAULT NULL,
  `action_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `call_actions`
--

CREATE TABLE IF NOT EXISTS `call_actions` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `call_actions`
--

INSERT INTO `call_actions` (`id`, `name`) VALUES
(1, 'Submitted'),
(2, 'Resolved'),
(3, 'Canceled'),
(4, 'Other');

-- --------------------------------------------------------

--
-- Структура таблицы `call_types`
--

CREATE TABLE IF NOT EXISTS `call_types` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `short_name` varchar(25) DEFAULT NULL,
  `icon_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `call_types`
--

INSERT INTO `call_types` (`id`, `name`, `short_name`, `icon_name`, `title`, `subtitle`) VALUES
(1, 'I''m in pain', 'Pain', 'headache.svg', 'Pain', 'I''m in'),
(2, 'I need the bathroom', 'Bathroom', 'toilet.svg', 'Bathroom', 'I need the'),
(3, 'I need a drink', 'Drink', 'glass-water.svg', 'Drink', 'I need a'),
(4, 'I have trouble with my My TV/Remote', 'TV/Remote', 'television.svg', 'TV/Remote', 'I have trouble with my'),
(5, 'I have a Question/Concern', 'Question', 'help.svg', 'Question/Concern', 'I have a ');

-- --------------------------------------------------------

--
-- Структура таблицы `facility`
--

CREATE TABLE IF NOT EXISTS `facility` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `facility`
--

INSERT INTO `facility` (`id`, `name`) VALUES
(1, 'Facility');

-- --------------------------------------------------------

--
-- Структура таблицы `patients`
--

CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL,
  `units_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `last_four_sn` varchar(4) DEFAULT NULL,
  `room_number` varchar(25) DEFAULT NULL,
  `patient_actions_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `patient_actions`
--

CREATE TABLE IF NOT EXISTS `patient_actions` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `patient_actions`
--

INSERT INTO `patient_actions` (`id`, `name`) VALUES
(1, 'Active'),
(2, 'Discharged'),
(3, 'Remove');

-- --------------------------------------------------------

--
-- Структура таблицы `SequelizeMeta`
--

CREATE TABLE IF NOT EXISTS `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `SequelizeMeta`
--

INSERT INTO `SequelizeMeta` (`name`) VALUES
('20180226103615-modify_call_types.js'),
('20180226105327-set_new_fields_call_types.js'),
('20180311111532-add_test_user.js'),
('20180412183006-change_type_last_four_sn.js'),
('20180418182525-change_patient_status.js'),
('20180428065327-add_column_reset_password.js'),
('20180502070530-create_subscription_table.js');

-- --------------------------------------------------------

--
-- Структура таблицы `units`
--

CREATE TABLE IF NOT EXISTS `units` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `facility_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `units`
--

INSERT INTO `units` (`id`, `name`, `facility_id`) VALUES
(1, 'Unit', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_roles_id` int(11) DEFAULT NULL,
  `user_statuses_id` int(11) DEFAULT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_expired` bigint(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_objects`
--

CREATE TABLE IF NOT EXISTS `user_objects` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `facility_id` int(11) DEFAULT NULL,
  `units_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_roles`
--

INSERT INTO `user_roles` (`id`, `name`) VALUES
(1, 'Administrator'),
(2, 'Nurse');

-- --------------------------------------------------------

--
-- Структура таблицы `user_statuses`
--

CREATE TABLE IF NOT EXISTS `user_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_statuses`
--

INSERT INTO `user_statuses` (`id`, `name`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Remove');

-- --------------------------------------------------------

--
-- Структура таблицы `user_subscriptions`
--

CREATE TABLE IF NOT EXISTS `user_subscriptions` (
  `id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `endpoint` varchar(255) DEFAULT NULL,
  `auth` varchar(255) DEFAULT NULL,
  `p256dh` varchar(255) DEFAULT NULL,
  `expired` bigint(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `calls`
--
ALTER TABLE `calls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_calls_patients_id` (`patients_id`),
  ADD KEY `FK_calls_users_id` (`action_by_users_id`),
  ADD KEY `FK_calls_call_types_id` (`call_types_id`),
  ADD KEY `FK_calls` (`call_actions_id`);

--
-- Индексы таблицы `call_actions`
--
ALTER TABLE `call_actions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `call_types`
--
ALTER TABLE `call_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_facility_name` (`name`);

--
-- Индексы таблицы `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_patients_units_id` (`units_id`),
  ADD KEY `FK_patients_patient_actions_id` (`patient_actions_id`);

--
-- Индексы таблицы `patient_actions`
--
ALTER TABLE `patient_actions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `SequelizeMeta`
--
ALTER TABLE `SequelizeMeta`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_units` (`facility_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_users_user_roles_id` (`user_roles_id`),
  ADD KEY `FK_users_user_statuses_id` (`user_statuses_id`);

--
-- Индексы таблицы `user_objects`
--
ALTER TABLE `user_objects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_user_objects` (`users_id`,`facility_id`,`units_id`),
  ADD KEY `FK_user_objects_facility_id` (`facility_id`),
  ADD KEY `FK_user_objects_units_id` (`units_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_statuses`
--
ALTER TABLE `user_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`),
  ADD KEY `user_subscriptions_endpoint` (`endpoint`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `calls`
--
ALTER TABLE `calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `call_actions`
--
ALTER TABLE `call_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `call_types`
--
ALTER TABLE `call_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `patient_actions`
--
ALTER TABLE `patient_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user_objects`
--
ALTER TABLE `user_objects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `user_statuses`
--
ALTER TABLE `user_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user_subscriptions`
--
ALTER TABLE `user_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
