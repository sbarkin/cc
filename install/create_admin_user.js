const args = process.argv;
if (args.length !== 6) {
    console.log('Please use format: create_admin_user first_name last_name email password');
    process.exit(1);
}

let models = require('./../models');
const Op = models.Sequelize.Op;
var commonService = require('./../lib/common.service');

const first_name = args[2];
const last_name = args[3]
const email = args[4];
const password = args[5];

commonService.setHash(password)
    .then(hash => {
        return models.users.create({
            first_name: first_name,
            last_name: last_name,
            email: email,
            password: hash,
            user_roles_id: 1,
            user_statuses_id: 1
        });
    })
    .then(result => {
        console.log('User was created successfully');
        process.exit()
    }).catch(err => {
        console.log('error created user', err);
        process.exit(1);
    });