'use strict';

let apiServer = require('./lib/apiServer');
global.socketServer = require('./lib/socketServer');

apiServer.start();
socketServer.start();