let env = process.env.NODE_ENV || 'development';
let config = require('./config.json')[env];

module.exports = config